.. Katsura documentation master file, created by
   sphinx-quickstart on Fri Nov 29 15:37:11 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Katsura's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   configuration
   modules/index
   API reference <apiref/modules>

Welcome to Katsura! Katsura is a Discord bot that has started as a simple bot
for the `TTT`_ Discord but has since evolved to take on many more features and
modules that serve either a useful purpose or are just used for fun.

This documentations gives an overview about the architecture of Katsura (for
developers), but also contains information about the usage of the bot (for
users) and on how to set it up. Furthermore, it contains a complete
`API reference <apiref/modules.html>`_ with documentation generated from the
Python docstrings.

Note that for a lot of commands, you can also get help on Discord by using
``!help COMMAND``.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _TTT: https://gw2ttt.com
