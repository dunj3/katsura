katsura package
===============

Subpackages
-----------

.. toctree::

   katsura.commands

Submodules
----------

katsura.config module
---------------------

.. automodule:: katsura.config
   :members:
   :undoc-members:
   :show-inheritance:

katsura.mathparser module
-------------------------

.. automodule:: katsura.mathparser
   :members:
   :undoc-members:
   :show-inheritance:

katsura.module module
---------------------

.. automodule:: katsura.module
   :members:
   :undoc-members:
   :show-inheritance:

katsura.permissions module
--------------------------

.. automodule:: katsura.permissions
   :members:
   :undoc-members:
   :show-inheritance:

katsura.utils module
--------------------

.. automodule:: katsura.utils
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: katsura
   :members:
   :undoc-members:
   :show-inheritance:
