katsura.commands package
========================

Submodules
----------

katsura.commands.antispam module
--------------------------------

.. automodule:: katsura.commands.antispam
   :members:
   :undoc-members:
   :show-inheritance:

katsura.commands.coffee module
------------------------------

.. automodule:: katsura.commands.coffee
   :members:
   :undoc-members:
   :show-inheritance:

katsura.commands.dice module
----------------------------

.. automodule:: katsura.commands.dice
   :members:
   :undoc-members:
   :show-inheritance:

katsura.commands.grrman module
------------------------------

.. automodule:: katsura.commands.grrman
   :members:
   :undoc-members:
   :show-inheritance:

katsura.commands.hangman module
-------------------------------

.. automodule:: katsura.commands.hangman
   :members:
   :undoc-members:
   :show-inheritance:

katsura.commands.maths module
-----------------------------

.. automodule:: katsura.commands.maths
   :members:
   :undoc-members:
   :show-inheritance:

katsura.commands.reactionrole module
------------------------------------

.. automodule:: katsura.commands.reactionrole
   :members:
   :undoc-members:
   :show-inheritance:

katsura.commands.reminder module
--------------------------------

.. automodule:: katsura.commands.reminder
   :members:
   :undoc-members:
   :show-inheritance:

katsura.commands.serversettings module
--------------------------------------

.. automodule:: katsura.commands.serversettings
   :members:
   :undoc-members:
   :show-inheritance:

katsura.commands.shipper module
-------------------------------

.. automodule:: katsura.commands.shipper
   :members:
   :undoc-members:
   :show-inheritance:

katsura.commands.taxis module
-----------------------------

.. automodule:: katsura.commands.taxis
   :members:
   :undoc-members:
   :show-inheritance:

katsura.commands.totos module
-----------------------------

.. automodule:: katsura.commands.totos
   :members:
   :undoc-members:
   :show-inheritance:

katsura.commands.trivia module
------------------------------

.. automodule:: katsura.commands.trivia
   :members:
   :undoc-members:
   :show-inheritance:

katsura.commands.wurm module
----------------------------

.. automodule:: katsura.commands.wurm
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: katsura.commands
   :members:
   :undoc-members:
   :show-inheritance:
