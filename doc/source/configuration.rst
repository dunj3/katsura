Configuration
=============

When started, the bot looks for configuration at ``katsura.yml``, which is a
YAML file. This file controls a lot about how the bot works. It contains the
Discord API token, the database URI, the list of active modules, but also
configuration about each module.

Note that each module can decide for itself if the configuration should be
placed in the YAML file or be dynamically loaded through the database.

The basic configuration looks like this:

.. code-block:: yaml

   bot:
     prefix: "!"
     logfile: "katsura.log"
     commands: []
     modules:
       - ".commands.serversettings.ServerSettings"
     setup_routines: []
     default_helptopic: "help"

   database:
     shelvedir: "shelves"
     uri: "sqlite:///katsura.sqlite3"

   api:
     discord: "INSERT CLIENT SECRET HERE"

This is not the most minimal configuration yet: ``bot.logfile``,
``bot.default_helptopic`` and ``database.shelvedir`` can be omitted, in which
case their default values (as seen above) will be used.

The configuration does can include values that are unused and no specific
structure is imposed by Katsura itself. Whether a module can make use of some
given configuration data is up to the module. Additional or un-used keys are
silently ignored.

Command line arguments
----------------------

You can instruct Katsura to use a different configuration file by supplying the
``--config`` command line parameter::

   python -m katsura --config=alternate-config.yml

Furthermore, you can enable debug output by providing the ``--debug`` switch.
Be warned though that this might generate a lot of noise!

To see all available command line switches, run the following command::

   python -m katsura --help

Configuration keys
------------------

``bot.prefix``
   The command prefix.

   This is a regular expression which must match the beginning of any string
   that should be considered a command for the bot.

``bot.logfile``
   The location of the logfile.

``bot.commands``
   A list of commands that should be loaded.

   Relative imports are resolved relative to the ``katsura`` package.

``bot.modules``
   A list of modules that should be loaded. Must refer to the actual module
   classes.

   Relative imports are resolved relative to the ``katsura`` package.

``bot.setup_routines``
   A list of setup routines that the bot should execute on startup.

``bot.default_helptopic``
   The default help topic to use when ``!help`` is called without arguments.

``database.shelvedir``
   A directory where the shelves should be placed.

   Will be created if it doesn't exist.

``database.uri``
   URI for the SQL database. Will be fed to SQLAlchemy.

``api.discord``
   Discord client token. Keep that one secret!
