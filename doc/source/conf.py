# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Katsura'
copyright = '2019, Peter Parker IV'
author = 'Peter Parker IV'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx.ext.intersphinx',
    'sphinx_autodoc_typehints',
    'sphinx_usagedoc',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# -- Intersphinx configuration -----------------------------------------------

# Links to foreign documentations
intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
    'sqlalchemy': ('https://docs.sqlalchemy.org/en/latest/', None),
    'discord': ('https://discordpy.readthedocs.io/en/latest/', None),
}


# -- Autodoc data truncation -------------------------------------------------
# Without this, some module constants will just eat up a lot of space because
# their whole repr() is included in the documentation

from sphinx.ext.autodoc import DataDocumenter, ModuleLevelDocumenter, SUPPRESS
from sphinx.util.inspect import object_description

def add_directive_header(self, sig: str) -> None:
    ModuleLevelDocumenter.add_directive_header(self, sig)
    sourcename = self.get_sourcename()
    if not self.options.annotation:
        try:
            objrepr = object_description(self.object)
            # Patched here:
            if len(objrepr) > 50:
                objrepr = objrepr[:50] + "\N{Horizontal Ellipsis}"
        except ValueError:
            pass
        else:
            self.add_line('   :annotation: = ' + objrepr, sourcename)
    elif self.options.annotation is SUPPRESS:
        pass
    else:
        self.add_line('   :annotation: %s' % self.options.annotation,
                      sourcename)

DataDocumenter.add_directive_header = add_directive_header
