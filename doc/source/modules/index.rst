Module overview
===============

For historical reasons, Katsura distinguishes between "commands" and "modules".
Commands are supposed to be small functions which operate without state, while
modules are supposed to provide more functionality at the expense of being
more cumbersome to write.

As a result, commands have the following limitations compared to modules:

* Cannot define their own database tables.
* Cannot use shelves for semi-persistent data.
* Cannot use event handlers to react to anything else than the command's
  invocation.
* Can only use global state.

.. note:: Since modules have not been part of Katsura from the beginning, some
   early commands do indeed rely on global state. New modules should always be
   written with the correct separation in mind though.

Each command/module has their own documentation here:

.. toctree::
   :maxdepth: 2
   :caption: Commands:

   serversettings
   taxis
   reactionrole

Developing your own module
--------------------------
