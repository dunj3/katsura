ReactionRole module
===================

The ReactionRole module provides a way to automatically assign roles to users
when they react to a message with a predefined emoji. It will also
automatically remove the role again when the user removes their reaction.

Configuration
-------------

The configuration for the reaction roles looks like this:

.. code:: yaml

   reactionroles:
     - guild: 593051013204803587
       message: 593276103364444210
       emoji: LFG
       role: LFC

``reactionroles`` is a list where each item contains the numeric guild ID, the
message ID, the emoji (as a name, without the colons) and the role (as a name).

.. note:: Even though the message can be identified with just the message ID,
   currently the guild ID also needs to be supplied.
