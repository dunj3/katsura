Taxi module
===========

The taxi module has been developed in order to facilitate the creation of the
"taxi/sqjoin" copy-and-paste snippets for TTT. It takes in a list of
commanders, and it produces output similar to the following, which can be
pasted to Discord and TeamSpeak::

   Discord taxis:
   **Amber** :WurmAttractant: :CommiYellow: :say_chat_run: `/sqjoin Jukommander`
   **Crimson** :CrimsonPhytotoxin: :CommiWhite: :say_chat_run: `/sqjoin Snackmander`
   **Cobalt** :Keg: :CommiBlue: :say_chat_run: `/sqjoin Yaru Lanayru`

   TeamSpeak taxis:
   [b][color=#F5E300]Amber[/color] (say-chat run)[/b]: /sqjoin Jukommander
   [b][color=#B9C2E2]Crimson[/color] (say-chat run)[/b]: /sqjoin Snackmander
   [b][color=#008BFF]Cobalt[/color] (say-chat run)[/b]: /sqjoin Yaru Lanayru

For historical reasons, taxis are implemented as a command in the
:mod:`katsura.commands.taxis` module.

Configuration
-------------

The configuration of the taxi module looks like this:

.. code-block:: yaml

   commanders:
     taxis:
       peter: "Cobalt Taxi"
       neko: "Amber Taxi"
       juko:
         _: "Jukommander"
         Cobalt: "Jukobalt Powderkeg"

   catchphrases:
     - "Fasten your seatbelts, we're about to head off to Bloodtide Coast!"
     - "Get ready for wurm killing action in T-10..."

In general, ``commanders.taxis`` is a dictionary that maps a commander's
(nick)name to the taxi character. If you want to use a specific taxi character
for a specific wurm head, it can be specified as a sub-dictionary. In that
case, ``_`` will be used to determine the default/fallback taxi name.

Additionally, ``catchphrases`` is a list of predefined catchphrases that will
be used if no user catchphrase is provided.

If you want to use the taxi web application, you should also add
``".commands.taxis.start_web_app"`` as a setup routine to the bot
configuration.

Note that the module also contains some hard-coded constants. In particular,
the following values are currently not changeable via the configuration file:

* Embed footer text.
* Embed footer image.
* Embed thumbnail.
* Embed message.
* Target channel for embeds.
* Target channel for web-app taxis.
* URL of the public web application.

Taxi usage
----------

The main command is ``!taxis``, which is implemented in
:meth:`~katsura.commands.taxis.generate_taxis`. It can operate in two modes:
Either by taking all commanders as arguments to the command itself, or by
starting the web application.

In the first case, the command takes in the Amber, the Crimson and the Cobalt
commander, in that order (for more information refer to the Discord help text).
It saves this information on a per-channel basis, so that you can further use
``!catchphrase`` and ``!post-taxis`` to create and post the embed to the
correct Discord channel.

In the second case, the bot creates and saves a token and returns a link to the
web application.
