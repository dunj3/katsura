ServerSettings module
=====================

The ServerSettings module provides a generic way to allow server specific
overrides of setting values, or for modules to provide an easy way to have
server-specific option values without the need to keep track of them on their
own.

This comes at a small cost however:

* The settings don't allow for a fixed schema, as everything is serialized to
  JSON.
* Modules have to explicitely make calls to the
  :class:`~katsura.commands.serversettings.ServerSettings` instance.

The last part is important, as it means that code reading configuration must
not solely rely on :class:`~katsura.config.Config`.

Usage
-----

On the client side, the module can be used with the ``!serverset`` command,
which allows you to either check, remove or set the server-specific
configuration value. For example, the bot has built-in support for changing the
command prefix, which can be used like this::

   !serverset bot.prefix.override "-"

In your modules, the server settings instance is best accessed by using the
:attr:`katsura.Katsura.server_settings` attribute. You can then use the API
methods documented for :class:`katsura.commands.serversettings.ServerSettings`.

Data validation
---------------

To prevent users from setting invalid configuration values and maybe even
breaking the bot in the process, there is a way to add validation callbacks
through
:meth:`~katsura.commands.serversettings.ServerSettings.add_validation_hook`,
which should be called in a module's :meth:`~katsura.module.Module.setup`
routine.

The name has to be the configuration value name, and the callback is a function
taking three parameters and returning a list of errors. An empty list means
that there were no validation errors.

The three parameters to the validation hook are the server ID, the setting name
and the setting value (already deserialized):

.. code:: python

   def validate_month(server_id, name, value):
       if 1 <= value <= 12:
           return []
        else:
            return ["Month must be between 1 and 12"]
