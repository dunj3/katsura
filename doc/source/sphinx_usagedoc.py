"""
    sphinx_usagedoc
    ~~~~~~~~~~~~~~~~~~~

    Converts Discord markdown to rST for certain docstrings.

    This is used to format command docstrings (which are written in Markdown to
    be sent to Discord) to rST to be included in the API reference.
"""

from typing import Any, Dict, List

from sphinx import __display_version__ as __version__
from sphinx.application import Sphinx

import m2r

from katsura.commands import get_command_name


def setup(app: Sphinx) -> Dict[str, Any]:
    """Sphinx extension setup function.

    When the extension is loaded, Sphinx imports this module and executes
    the ``setup()`` function, which in turn notifies Sphinx of everything
    the extension offers.

    Parameters
    ----------
    app : sphinx.application.Sphinx
        Application object representing the Sphinx process

    See Also
    --------
    `The Sphinx documentation on Extensions
    <http://sphinx-doc.org/extensions.html>`_

    `The Extension Tutorial <http://sphinx-doc.org/extdev/tutorial.html>`_

    `The Extension API <http://sphinx-doc.org/extdev/appapi.html>`_

    """
    if not isinstance(app, Sphinx):
        # probably called by tests
        return {'version': __version__, 'parallel_read_safe': True}

    app.setup_extension('sphinx.ext.autodoc')
    app.connect('autodoc-process-docstring', _process_docstring)

    return {'version': __version__, 'parallel_read_safe': True}


def _process_docstring(app: Sphinx, what: str, name: str, obj: Any,
                       options: Any, lines: List[str]) -> None:
    """Process the docstring for a given python object.

    Called when autodoc has read and processed a docstring. `lines` is a list
    of docstring lines that `_process_docstring` modifies in place to change
    what Sphinx outputs.

    Parameters
    ----------
    app : sphinx.application.Sphinx
        Application object representing the Sphinx process.
    what : str
        A string specifying the type of the object to which the docstring
        belongs. Valid values: "module", "class", "exception", "function",
        "method", "attribute".
    name : str
        The fully qualified name of the object.
    obj : module, class, exception, function, method, or attribute
        The object to which the docstring belongs.
    options : sphinx.ext.autodoc.Options
        The options given to the directive: an object with attributes
        inherited_members, undoc_members, show_inheritance and noindex that
        are True if the flag option of same name was given to the auto
        directive.
    lines : list of str
        The lines of the docstring, see above.

        .. note:: `lines` is modified *in place*

    """
    cmd_name = get_command_name(obj)
    if cmd_name is None:
        return

    text = '\n'.join(lines)
    rst = m2r.convert(text)

    result_lines = [
        f'| *This function is available as a command:* ``{cmd_name}``.',
        f'| You can use ``!help {cmd_name}`` to get this information '
        'on Discord.',
    ]
    result_lines.extend(rst.split('\n'))

    lines[:] = result_lines[:]
