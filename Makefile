tests:
	.venv/bin/python -m pytest

lint: pylint flake8

pylint:
	.venv/bin/pylint katsura || true

flake8:
	.venv/bin/flake8 katsura || true
