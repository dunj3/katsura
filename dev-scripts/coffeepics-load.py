import sys
import os
import requests
import subprocess
import re

from io import BytesIO

def next_num():
    files = os.listdir()
    numbers = []
    for name in files:
        m = re.match('(\\d+)\\.jpg', name)
        if m:
            numbers.append(int(m.group(1)))
    return max(numbers) if numbers else 0


def scrape(url, num):
    print("> Downloading", url)
    try:
        handle = requests.get(url)
    except Exceptiom as e:
        print("FAILED", str(e))
    outname = '{}.jpg'.format(num)
    proc = subprocess.Popen(['convert', '-', '-resize', '1000x1000>', outname], stdin=subprocess.PIPE)
    proc.stdin.write(handle.content)
    proc.stdin.close()
    proc.wait()
    if proc.returncode != 0:
        print("Failed!")
        return False
    return True


urls = sys.stdin.read().split("\n")

num = next_num()

for url in urls:
    if not url:
        continue
    if scrape(url, num):
        num += 1
