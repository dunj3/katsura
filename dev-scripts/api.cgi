#!/usr/bin/python3
# This is a "quick and dirty" API CGI script to receive coffee pics. The idea
# is simple: We can separate the coffeepic hosting from the Discord bot, for
# example if the Discord bot runs locally or on a different server. There's
# only two commands really necessary, one to upload a file and one to list all
# images. In both cases, the return value is JSON-encoded and easy to consume
# for Katsura. The secret should be well chosen to avoid opening up a DoS
# attack vector, as not much verification is done here - we rely on the bot to
# do the heavy lifting.
#
# Note that this script has some requirements:
#   pip install imagehash pillow multipart
#
# And of course, it must actually run as a CGI script.
print("Content-type: application/json")

# Adjust your settings here:
GLOB_PATTERN = "*.jpg"
IMAGE_EXT = ".jpg"
CACHE_FILE = "/tmp/coffeepics-upload.cache"
SECRET = b""


import os
import io
import sys
import json
import glob
import uuid
import multipart
import imagehash

from PIL import Image


if not SECRET:
    print("Status: 500")
    print()
    print(json.dumps({
        "error": "No secret set!",
    }))
    sys.exit(0)


def load_cache():
    try:
        with open(CACHE_FILE) as f:
            return json.load(f)
    except:
        return {}


def save_cache():
    with open(CACHE_FILE, "w") as f:
        json.dump(cache, f)


cache = load_cache()


def load_hash(filename):
    if filename in cache:
        return cache[filename]

    img_hash = imagehash.average_hash(Image.open(filename))
    img_hash = img_hash.hash.tolist()

    cache[filename] = img_hash
    return cache[filename]


content_type = os.getenv('CONTENT_TYPE')
content_type, options = multipart.parse_options_header(content_type)

def main(keys):
    if keys.get("SECRET") != SECRET:
        print("Status: 401")
        print()
        print(json.dumps({
            "error": "Invalid secret",
        }))
        return

    action = keys.get("ACTION")
    if action == b"build-cache":
        for name in glob.glob(GLOB_PATTERN):
            load_hash(name)
        print("Status: 200")
        print()
        print(json.dumps({
            "success": "Cache built",
        }))
    elif action == b"list":
        names = glob.glob(GLOB_PATTERN)
        print("Status: 200")
        print()
        print(json.dumps({
            "images": names,
        }))
    elif action == b"upload" and "IMAGE" in keys:
        # Check if this is a duplicate
        img_data = io.BytesIO(keys["IMAGE"])
        image = Image.open(img_data)
        new_hash = imagehash.average_hash(image).hash.tolist()

        for existing in glob.glob(GLOB_PATTERN):
            this_hash = load_hash(existing)
            if new_hash == this_hash:
                print("Status: 400")
                print()
                print(json.dumps({
                    "error": "Image exists",
                }))
                return

        # We made it so far, so save the image already
        name = str(uuid.uuid4()) + IMAGE_EXT
        with open(name, "wb") as fo:
            fo.write(keys["IMAGE"])
        print("Status: 200")
        print()
        print(json.dumps({
            "success": "Image uploaded",
            "filename": name,
        }))
    else:
        print("Status: 400")
        print()
        print(json.dumps({
            "error": "Expected a proper action",
        }))

if content_type == "multipart/form-data" or os.getenv("REQUEST_METHOD") != "POST":
    boundary = options.get("boundary")
    keys = {}
    for part in multipart.MultipartParser(sys.stdin.buffer, boundary, -1):
        keys[part.name] = part.raw
    main(keys)
else:
    print("Status: 400")
    print()
    print(json.dumps({
        "error": "Expected POSTed multipart/form-data",
    }))

if cache:
    save_cache()
