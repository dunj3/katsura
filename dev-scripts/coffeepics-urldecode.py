from urllib.parse import urlparse, parse_qs

def extract_url(x):
    return parse_qs(urlparse(x).query).get('imgurl', [None])[0]

while True:
    try:
        line = input()
    except EOFError:
        break
    url = extract_url(line)
    if url:
        print(url)
