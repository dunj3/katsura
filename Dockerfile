FROM python:3.10
COPY poetry.lock pyproject.toml MANIFEST.in README.md /build/
COPY katsura /build/katsura
RUN pip install file:////build/

WORKDIR /katsura
CMD ["python", "-m", "katsura", "--config", "katsura.yml"]
