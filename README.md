# Katsura Discord Bot

This is the code of the Discord bot that powered Wurmonator on TTT, and Caffeinator on Chatterbrains.

**Note**: The code is mostly here for personal reasons. If you're looking to build a Discord bot yourself, check out tutorials and use properly maintained libraries (like discordpy's [`discord.ext.commands`](https://discordpy.readthedocs.io/en/stable/ext/commands/index.html))

**Note²**: Keep in mind that Discord is not a private or secure messaging service. Treat it more like a social network, and don't use it to share sensitive information. If you're looking for a secure chat platform that you can write bots for, why not give [Matrix](https://matrix.org/) a shot? A [Python library](https://github.com/matrix-org/matrix-python-sdk) is available.

## Usage

*It is advised that you do not use this bot unless you are me and know what you're getting into!*

Via virtualenvs:

```
virtualenv .venv
.venv/bin/pip install .
.venv/bin/python -m katsura --config=...
```

Via Docker/Podman:

```
docker build -t katsura .
mkdir bot-data
$EDITOR bot-data/katsura.yml
docker run --rm -ti -v ./bot-data:/katsura katsura
```

## License

```
Copyright 2022 Daniel Schadt

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
