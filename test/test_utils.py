import datetime

import pytest

from katsura import utils


@pytest.mark.parametrize("input, expected", [
    ([[1, 2]], [[1], [2]]),
    ([[1, 2], [3, 4]], [[1, 3], [2, 4]]),
    ([[1, 2, 3], [4, 5, 6]], [[1, 4], [2, 5], [3, 6]]),
])
def test_transposed(input, expected):
    assert utils.transposed(input) == expected


def test_randint_without():
    # Probabilistic test
    start = 1
    end = 20
    without = {3, 4, 5, 6}
    repeats = 100
    for _ in range(repeats):
        draw = utils.randint_without(start, end, without)
        assert start <= draw <= end
        assert draw not in without


@pytest.mark.parametrize("a, b, expected", [
    ("2019-01-02 01:00", "2019-01-01 01:00", 1),
    ("2019-01-02 23:00", "2019-01-01 01:00", 1),
    ("2019-01-02 01:00", "2019-01-02 10:00", 0),
    ("2019-01-10 01:00", "2019-01-01 23:00", 9),
])
def test_days_delta_between(a, b, expected):
    date_format = "%Y-%m-%d %H:%M"
    date_a = datetime.datetime.strptime(a, date_format)
    date_b = datetime.datetime.strptime(b, date_format)
    expected_delta = datetime.timedelta(days=expected)
    assert utils.days_delta_between(date_a, date_b) == expected_delta


@pytest.mark.parametrize("seconds, output", [
    (1, "1 second"),
    (10, "10 seconds"),
    (60, "1 minute"),
    (70, "1 minute 10 seconds"),
    (120, "2 minutes"),
    (121, "2 minutes 1 second"),
    (60 * 60, "1 hour"),
    (2 * 60 * 60 + 1, "2 hours 1 second"),
])
def test_human_timedelta(seconds, output):
    delta = datetime.timedelta(seconds=seconds)
    assert utils.human_timedelta(delta) == output


@pytest.mark.parametrize("seconds, expected", [
    (0.5, 0),
    (0.7, 0),
    (1.3, 1),
    (1.7, 1),
    (-0.5, 0),
    (-1.3, -1),
    (-1.7, -1),
])
def test_strip_to_seconds(seconds, expected):
    delta = datetime.timedelta(seconds=seconds)
    delta = utils.strip_to_seconds(delta)
    assert delta.total_seconds() == expected
    assert int(delta.total_seconds()) == delta.total_seconds()


@pytest.mark.parametrize("num, base, expected", [
    (0, 10, 1),
    (1, 10, 1),
    (9, 10, 1),
    (10, 10, 2),
    (19, 10, 2),
    (99, 10, 2),
    (100, 10, 3),

    (0, 16, 1),
    (1, 16, 1),
    (15, 16, 1),
    (16, 16, 2),
    (255, 16, 2),
    (256, 16, 3),
])
def test_count_digits(num, base, expected):
    assert utils.count_digits(num, base) == expected
