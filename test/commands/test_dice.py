import pytest

from katsura.commands import dice


class TestDiceRoll:
    REPEATS = 10

    @pytest.mark.parametrize("input", [
        [1, 2, 3],
        ["foo", "bar"],
    ])
    def test_draw_list_nonblock(self, input):
        roller = dice.DiceRoll(lst=input)
        for _ in range(self.REPEATS):
            item = roller.draw(block=False)
            assert item in input

    @pytest.mark.parametrize("start, end", [
        (1, 10),
        (2, 20),
    ])
    def test_draw_range_nonblock(self, start, end):
        roller = dice.DiceRoll(start=start, end=end)
        for _ in range(self.REPEATS):
            item = roller.draw(block=False)
            assert start <= item <= end

    def test_draw_range_block(self):
        start = 1
        end = 20
        blocked = set()
        roller = dice.DiceRoll(start=start, end=end)
        for _ in range(self.REPEATS):
            item = roller.draw()
            roller.block(item)
            assert item not in blocked
            blocked.add(item)

    def test_draw_list_repeated(self):
        lst = ["foo", "foo", "foo"]
        roller = dice.DiceRoll(lst=lst)

        for _ in range(len(lst)):
            item = roller.draw()
            roller.block(item)
            assert item in lst

        item = roller.draw()
        assert item is None
