import pytest

from katsura import permissions

@pytest.mark.parametrize("needle, haystack, expected", [
    (".foo", ".foo.bar", True),
    (".foo", ".fooo", False),
    (".foo.bar", ".foo.bar", True),
    (".foo", ".bar.foo", False),
])
def test_is_prefix(needle, haystack, expected):
    assert permissions.is_prefix(needle, haystack) == expected
