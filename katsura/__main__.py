"""Starting module for the bot."""

import logging
import argparse
import asyncio

from . import Katsura
from .config import Config

LOGGER = logging.getLogger(__name__)


def get_parser():
    """Builds and returns the command line argument parser."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--channel-whitelist",
        nargs="*",
        metavar="CHANNEL",
        help="Only allow commands in the given channels"
    )
    parser.add_argument(
        "--config",
        default="katsura.yml"
    )
    parser.add_argument(
        "--instance-name",
        help="Define a name for this instance that can be used to distinguish"
             " between multiple bot instances."
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Enable debug output"
    )
    parser.add_argument(
        "--clear-shelves",
        action="store_true",
        help="Clear all shelves before starting"
    )
    return parser


async def main():
    """Main entry point."""
    parser = get_parser()
    args = parser.parse_args()

    config = Config()
    config.load_from(args.config)

    logfile = config.get("bot.logfile", "katsura.log")
    log_format = "{asctime} {name} [{levelname}] {message} ({filename}:{funcName})"
    logging.basicConfig(
        filename=logfile,
        level=logging.INFO,
        style="{",
        format=log_format,
    )

    formatter = logging.Formatter(log_format, style="{")
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        logging.getLogger().setLevel(logging.INFO)
    logging.getLogger().addHandler(handler)

    LOGGER.info("Bot started")

    katsura = Katsura(args, config)
    if args.clear_shelves:
        katsura.clear_shelves()
    katsura.load_modules_from_config()
    katsura.setup()
    await katsura.setup_routines()
    await katsura.start(config.get("api.discord"))


if __name__ == "__main__":
    asyncio.run(main())
