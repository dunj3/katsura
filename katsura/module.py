"""Module system for the bot."""

import os
import dbm
import shelve
import logging

from . import utils

LOGGER = logging.getLogger(__name__)

DEFAULT_SHELVEDIR = "shelves"


class Module:
    """Represents a bot module."""

    def __init__(self, bot):
        self.bot = bot

    def setup(self):
        """Do the required module initialization."""

    def init_db(self, metadata):
        """Called to initialize possible database metadata.

        Args:
            metadata (sqlalchemy.schema.MetaData): The SQLalchemy metadata.
        """

    def shelve(self, name, modname=None, *, keyfn=None, default_factory=None):
        """Create and return a shelf for semipersistent data.

        Args:
            name (str): Name of the shelf.
            modname (str): Name of the module, defaults to __class__.__name__.
            keyfn: A function that is evaluated on the keys before looking them
                up.
            default_factory: A function that produces a default element for
                missing items.

        Returns:
           A shelf with a dict-like interface.
        """
        if modname is None:
            modname = self.__class__.__name__
        shelvedir = self.bot.config.get("database.shelvedir", DEFAULT_SHELVEDIR)
        fname = f"{modname}.{name}.shelf"
        fpath = os.path.join(shelvedir, fname)
        LOGGER.debug(f"Requested shelf for {modname}.{name}, looking at {fpath}")

        try:
            shelf = shelve.open(fpath, writeback=True)
        except dbm.error:
            LOGGER.info(f"Shelf {fpath} couldn't be opened, starting it over")
            os.unlink(fpath)
            shelf = shelve.open(fpath, writeback=True)
        return utils.AugmentedShelf(shelf, keyfn, default_factory)

    async def request_help(self, topic):
        """Return a help text for the given topic.

        This method is called by ``!help`` if a topic cannot be found by other
        built-in helps. This gives modules the ability to provide their own
        help texts, possibly generated dynamically.

        Args:
            topic (str): Requested help topic.

        Returns:
            str: Either the help text if it was found, or ``None``.
        """
        # pylint: disable=unused-argument
        return None
