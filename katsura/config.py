"""Configuration file handling.

The configuration file of Katsura is just one big YAML file. Every type of
value that can be serialized from/to YAML is therefore allowed in configuration
values.

The configuration is build hierarchical. The :class:`Config` object provides an
easy method to access nested objects.

This module currently does not support the programmatic modification of the
configuration, and neither does it support writing a configuration file.
Therefore, the configuration can be treated as a read-only,
initialized-at-startup thing.
"""

import yaml


class Config:
    """Represents a configuration file."""
    def __init__(self):
        self.data = {}

    def load_from(self, filename):
        """Load a config from the given filename.

        Args:
            filename (str): Filename to load the configuration from.
        """
        with open(filename, "r") as f:
            content = f.read()
        self.data = yaml.safe_load(content)

    def get(self, path, default=None):
        """Get a value from the config file.

        ``path`` may be a dotted path.

        Args:
            path (str): Path to the configuration value.
            default: Value to return if not set.

        Returns:
           Confugration value or default.
        """
        parts = path.split(".")
        cursor = self.data
        while parts:
            part = parts.pop(0)
            if part in cursor:
                cursor = cursor[part]
            else:
                return default
        return cursor
