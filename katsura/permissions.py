"""Permission system implementation."""


def is_prefix(needle, haystack, delimiter="."):
    """Checks if the given needle is a prefix of haystack.

    Unlike haystack.startswith(needle), this function splits the strings at the
    given delimiter and then checks if the resulting lists are a prefix of each
    other.

    Args:
        needle (str): Needle to search for.
        haystack (str): Haystack in which to search.
        delimiter (str): The delimiter.

    Returns:
        bool: True if needle is a prefix of haystack.
    """
    needle = needle.rstrip(".")
    haystack = haystack.rstrip(".")
    needle_list = needle.split(delimiter)
    haystack_list = haystack.split(delimiter)

    return needle_list == haystack_list[:len(needle_list)]


def contains_permission(perms, permission):
    """Check if the given set of permissions contains the permission."""
    for perm in perms:
        if perm == permission:
            return True
        if is_prefix(perm, permission):
            return True
    return False


class Permissions:
    """Represents a permission container."""

    def __init__(self, config):
        self.config = config

    def get_for(self, name):
        """Return all permissions for the given name."""
        return self.config.get("permissions", {}).get(name, [])

    def check(self, user, server, permission):
        """Check if the given user on the given server has the given
        permission."""
        username = f"{user.name}#{user.discriminator}"
        perms = self.get_for(username)
        if contains_permission(perms, permission):
            return True

        for role in user.roles:
            rolename = f"{role.name}@{server.id}"
            perms = self.get_for(rolename)
            if contains_permission(perms, permission):
                return True

        return False
