"""Parser and evaluator for mathematical expressions."""

# Pylint seems to get confused about Parser.next() not being callable, so
# disable the lint here.
# pylint: disable=not-callable

import re
import math
import types
import decimal
import inspect


class UnknownToken(Exception):
    """Error representing an unknown token.

    Attributes:
        text (str): Error description.
        original (str): Original input.
        position (int): Position in the input text.
    """

    def __init__(self, original, text, position):
        super().__init__()
        self.text = text
        self.original = original
        self.position = position

    def __str__(self):
        return self.text


class ParseError(Exception):
    """Error representing a parsing error.

    Attributes:
        text (str): Error description.
        position (int): Position in the input text.
    """

    def __init__(self, text, position=None):
        super().__init__()
        self.text = text
        self.position = position

    def __str__(self):
        return self.text


class RuntimeError(Exception):
    """Error during the evaluation of an expression.

    Attributes:
        msg (str): Error description.
        underlying (Exception): Underlying Python exception if available.
    """
    # pylint: disable=redefined-builtin

    def __init__(self, msg, underlying=None):
        super().__init__()
        self.msg = msg
        self.underlying = underlying

    def __str__(self):
        return self.msg


TOKENS = [
    ("FLOAT", r"\d+\.\d+"),
    ("INTEGER", r"\d+"),
    ("SYMBOL", "[A-Za-z]+"),
    ("COMMA", ","),
    ("PLUS", r"\+"),
    ("MINUS", "-"),
    ("POW", r"\*\*"),
    ("MULT", r"\*"),
    ("DIV", "/"),
    ("BANG", "!"),
    ("LPAREN", r"\("),
    ("RPAREN", r"\)"),
    (None, r"\s+"),
]


class Token:
    """Class for an input token after lexing.

    Attributes:
        typ (str): Token type.
        match: Match object.
        pos (int): Position in the input.
    """
    # pylint: disable=too-few-public-methods
    __slots__ = ("typ", "match", "pos")

    def __init__(self, typ, match, pos):
        self.typ = typ
        self.match = match
        self.pos = pos


def lex(text):
    """Lex the given text and return its tokens.

    Args:
        text (str): Input text to lex.

    Returns:
        generator: A generator of the input's tokens.
    """
    original = text
    position = 0
    while text:
        for ttype, tre in TOKENS:
            match = re.match(tre, text)
            if match:
                if ttype:
                    yield Token(ttype, match, position)
                text = text[match.end():]
                position += match.end()
                break
        else:
            raise UnknownToken(original, text, position)
    yield Token("EOF", None, position)


class Context:
    """Evaluation context for holding available variables."""
    # pylint: disable=no-self-use

    def __init__(self):
        self.globals = {}

    def get_global(self, name):
        """Return the global varaible with the given name."""
        return self.globals[name]

    def is_number(self, obj):
        """Check if the given object is a number."""
        return isinstance(obj, decimal.Decimal)

    def is_function(self, obj):
        """CCheck if the given object is a function."""
        return isinstance(obj, (types.FunctionType, types.BuiltinFunctionType))

    def format_result(self, obj):
        """Format the given object for human consumption.

        Args:
            obj: The object to format.

        Returns:
            str: The human readable version.
        """
        if self.is_number(obj):
            return str(obj)
        if self.is_function(obj):
            name = obj.__name__
            params = inspect.signature(obj).parameters
            param_str = ", ".join(params)
            return f"<function {name}({param_str})>"
        return repr(obj)


class Node:
    """Base class for AST nodes."""
    # pylint: disable=too-few-public-methods,unused-argument,no-self-use

    def eval(self, ctx):
        """Evaluate this ast node under the given context.

        Args:
            ctx (~katsura.mathparser.Context): The context to evaluate this in.

        Returns:
            The resulting object.
        """
        return NotImplemented


class Literal(Node):
    """A literal number.

    Attributes:
        value (str): The literal's value.
    """
    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return f"<Literal value={self.value!r}>"

    def eval(self, ctx):
        return decimal.Decimal(self.value)


class Addition(Node):
    """An addition.

    Attributes:
        parts (list): A list of summands.
    """

    def __init__(self, parts):
        self.parts = parts

    def __repr__(self):
        return f"<Addition parts={self.parts!r}>"

    def eval(self, ctx):
        first, *rest = self.parts
        value = first[1].eval(ctx)
        for func, ast in rest:
            assert func in {"PLUS", "MINUS"}
            current = ast.eval(ctx)

            if ctx.is_function(value) or ctx.is_function(current):
                raise RuntimeError("Can't add or subtract functions")

            if func == "PLUS":
                value += current
            elif func == "MINUS":
                value -= current
        return value


class Multiplication(Node):
    """A multiplication.

    Attributes:
        parts (list): A list of factors.
    """

    def __init__(self, parts):
        self.parts = parts

    def __repr__(self):
        return f"<Multiplication parts={self.parts!r}>"

    def eval(self, ctx):
        first, *rest = self.parts
        value = first[1].eval(ctx)
        for func, ast in rest:
            assert func in {"MULT", "DIV"}
            current = ast.eval(ctx)

            if ctx.is_function(value) or ctx.is_function(current):
                raise RuntimeError("Can't multiply or divide functions")

            if func == "MULT":
                value *= current
            elif func == "DIV":
                if current == decimal.Decimal(0):
                    raise RuntimeError("division by zero - we have just created a black hole.")
                value /= current
        return value


class Exponentiation(Node):
    """An exponentiation.

    Attributes:
        parts (list): A list of powers.
    """

    def __init__(self, parts):
        self.parts = parts

    def __repr__(self):
        return f"<Exponentiation parts={self.parts!r}>"

    def eval(self, ctx):
        first, *rest = self.parts
        value = first.eval(ctx)
        for part in rest:
            current = part.eval(ctx)
            if ctx.is_function(value) or ctx.is_function(current):
                raise RuntimeError("Can't exponentiate functions")
            value **= current
        return value


class Negation(Node):
    """Negation of a specific value.

    Attributes:
        value (Node): The value to be negated.
    """

    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return f"<Negation value={self.value!r}>"

    def eval(self, ctx):
        value = self.value.eval(ctx)
        if ctx.is_function(value):
            raise RuntimeError("Can't negate functions")
        return -value


class Symbol(Node):
    """A symbol/identifier.

    Attributes:
        name (str): Name of the symbol.
    """

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return f"<Symbol name={self.name}>"

    def eval(self, ctx):
        try:
            return ctx.get_global(self.name)
        except KeyError:
            raise RuntimeError(f"Variable {self.name} not found") from None


def _make_multiplier(factor):
    return lambda x: factor * x


class Call(Node):
    """A function call.

    Attributes:
        func: The function.
        params: List of parameters.
    """

    def __init__(self, func, params):
        self.func = func
        self.params = params

    def __repr__(self):
        return f"<Call func={self.func!r} params={self.params!r}>"

    def eval(self, ctx):
        func = self.func.eval(ctx)
        if not ctx.is_function(func):
            func = _make_multiplier(func)

        signature = inspect.signature(func)
        if len(signature.parameters) != len(self.params):
            raise RuntimeError("Invalid number of arguments for {}"
                               .format(ctx.format_result(func)))

        params = [p.eval(ctx) for p in self.params]
        try:
            return func(*params)
        except Exception as exc:
            raise RuntimeError("Error while executing {}({})".format(
                ctx.format_result(func),
                ", ".join(ctx.format_result(param) for param in params)
            ), exc)


class Factorial(Node):
    """A factorial.

    Attributes:
        inner: Value that the factorial should be calculated of.
    """

    def __init__(self, inner):
        self.inner = inner

    def __repr__(self):
        return f"<Factorial inner={self.inner}>"

    def eval(self, ctx):
        inner = self.inner.eval(ctx)
        if not ctx.is_number(inner):
            raise RuntimeError("Can't take factorial of functions")
        return _factorial(inner)


def _factorial(x):
    """Factorial for integer decimals."""
    if x != x.to_integral_value():
        raise RuntimeError("Non-integer factorial")
    accum = 1
    while x:
        accum *= x
        x -= 1
    return accum


class Parser:
    """A parser that takes tokens and prodcues an AST."""

    def __init__(self, tokens):
        self.tokens = tokens

    def next(self):
        """Delete and return the next token."""
        return self.tokens.pop(0)

    def peek(self):
        """Return the next token without deleting it."""
        return self.tokens[0]

    def expect(self, ttype):
        """Make sure the next token is of the given type.

        Raises a ParseError if the token type doesn't match.
        """
        if not self.tokens or self.peek().typ != ttype:
            position = None if not self.tokens else self.peek().position
            raise ParseError(f"Expected {ttype}", position)
        self.next()

    def parse(self):
        """Run the parser and return the result.

        This function also ensures that all of the input is consumed and there
        are no leftover tokens.

        Raises:
            ParseError: If an invalid input token stream is given.
        """
        result = self.parse_expression()
        if self.tokens and self.peek().typ != "EOF":
            raise ParseError("Leftover tokens", self.peek().pos)
        return result

    def parse_expression(self):
        """Parse a single expression."""
        return self.parse_addition()

    def parse_addition(self):
        """Parse an addition."""
        adds = []
        direction = None
        while True:
            expr = self.parse_multiplication()
            adds.append((direction, expr))
            if self.peek().typ not in {"PLUS", "MINUS"}:
                break
            direction = self.next().typ
        return Addition(adds)

    def parse_multiplication(self):
        """Parse a multiplication."""
        factors = []
        direction = None
        while True:
            expr = self.parse_power()
            factors.append((direction, expr))
            if self.peek().typ not in {"MULT", "DIV"}:
                break
            direction = self.next().typ
        return Multiplication(factors)

    def parse_power(self):
        """Parse an exponentiation."""
        exponents = []
        while True:
            expr = self.parse_atom()
            exponents.append(expr)
            if self.peek().typ != "POW":
                break
            self.next()
        return Exponentiation(exponents)

    def parse_atom(self):
        """Parse an atom."""
        if self.peek().typ == "MINUS":
            self.next()
            return Negation(self.parse_atom())

        atom = None
        next_token = self.next()
        if next_token.typ in {"INTEGER", "FLOAT"}:
            atom = Literal(next_token.match.group())
        elif next_token.typ == "SYMBOL":
            atom = Symbol(next_token.match.group())
        elif next_token.typ == "LPAREN":
            atom = self.parse_expression()
            self.expect("RPAREN")
        else:
            raise ParseError("Unpexected token", next_token.pos)

        while True:
            if self.peek().typ == "LPAREN":
                trailer = self.get_trailer()
                atom = Call(atom, trailer)
            elif self.peek().typ == "BANG":
                self.next()
                atom = Factorial(atom)
            else:
                break
        return atom

    def get_trailer(self):
        """Get the trailer for an expression."""
        self.expect("LPAREN")
        result = []
        while True:
            if self.peek().typ == "RPAREN":
                self.next()
                return result
            result.append(self.parse_expression())
            if self.peek().typ not in {"COMMA", "RPAREN"}:
                raise ParseError("Arguments must be separated by comma",
                                 self.peek().pos)
            if self.peek().typ == "COMMA":
                self.next()


def _f(name, function):
    function.__name__ = name
    return function


def default_context():
    """Returns the default context with default variables."""
    context = Context()
    context.globals = {
        "pi": decimal.Decimal(math.pi),
        "e": decimal.Decimal(math.e),
        "abs": abs,
        "sin": _f("sin", lambda r: decimal.Decimal(math.sin(r))),
        "cos": _f("cos", lambda r: decimal.Decimal(math.cos(r))),
        "tan": _f("tan", lambda r: decimal.Decimal(math.tan(r))),
        "exp": _f("exp", lambda x: decimal.Decimal(math.e) ** x),
        "log": _f("log", lambda x: decimal.Decimal(math.log(x))),
        "logb": _f("logb", lambda x, base: decimal.Decimal(math.log(x, base))),
    }
    return context


def parse(text):
    """Parse the given text to an AST.

    Args:
        text (str): The text to parse.

    Returns:
        The AST.

    Raises:
        ParseError
    """
    tokens = list(lex(text))
    parser = Parser(tokens)
    return parser.parse()


def eval_str(text, context=None):
    """Parse and evaluate the given text.

    Args:
        text (str): Text to evaluate.
        context (~katsura.mathparser.Context): The context to evaluate in. None
            to create a new default context.

    Returns:
        The evaluation result.

    Raises:
        ParseError: If text is malformed.
        RuntimeError: If the result cannot be calculated.
    """
    if context is None:
        context = default_context()
    ast = parse(text)
    return ast.eval(context)


def repl(*, prompt="#>"):
    """Run a read-eval-print loop.

    This function should only be used for developing/debugging purposes.

    It does not return anything and just blocks until the user exits the loop.

    Args:
        prompt (str): The prompt to print before each input.
    """
    # pylint: disable=broad-except
    context = default_context()
    while True:
        line = input(prompt)
        try:
            print(context.format_result(eval_str(line, context)))
        except UnknownToken as exc:
            prefix = "Unknown token:"
            print(prefix, exc.original)
            print(" " * len(prefix), " " * exc.position + "^")
        except ParseError as exc:
            print("Parse error:", exc)
            if exc.position is not None:
                print(line)
                print(" " * exc.position + "^")
        except Exception:
            import traceback
            traceback.print_exc()
