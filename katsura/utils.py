"""General utility functions."""
import datetime
import textwrap
import re
import random
import os
from operator import attrgetter

import grapheme
import discord
from discord.utils import get

TIME_UNIT_FACTOR = 60

USER_MENTION_RE = re.compile(r"<@!?(\d+)>")


def get_emoji(server, name):
    """Returns the server-specific emoji name."""
    if server is None:
        return None
    emoji = get(server.emojis, name=name)
    if emoji is None:
        return None
    return f"<:{emoji.name}:{emoji.id}>"


def replace_server_emojis(text, server, fallback=lambda name: f":{name}:"):
    """Replaces emojis with their server-specific placeholder.

    Args:
        text (str): The text in which to replace emojis.
        server (discord.Server): The server for which to replace emojis.
        fallback (str -> str): Function which will be called for missing emoji.

    Returns:
        str: The string with server-specific emojis replaced.
    """
    def replacer(match):
        name = match.group(1)
        emoji = get_emoji(server, name)
        if emoji is None:
            return fallback(name)
        return emoji

    return re.sub(
        r"(?<!<):([A-Za-z_]+):(?!\d+:>)",
        replacer, text
    )


def find_user_by_nick(bot, name, *, server=None):
    """Find a user by the given name.

    If server is given, it will only search the members of the given server.

    Args:
        bot (discord.Client): The bot client.
        name (str): The user name or nickname.
        server (discord.Server): The server for which nicknames should be
            considered.

    Returns:
        discord.User: The user with the given name.
    """
    if server is None:
        members = bot.get_all_members()
    else:
        members = server.members
    for member in members:
        if name.lower() == member.name.lower():
            return member

        if (server is not None
                and member.nick is not None
                and member.nick.lower() == name.lower()):
            return member
    return None


def is_user_mention(text):
    """Checks if the given text is a direct user mention.

    Args:
        text (str): The text.

    Returns:
       int: Either the user ID that was mentioned, or None.
    """
    match = USER_MENTION_RE.match(text)
    if match:
        return int(match.group(1))
    return None


def find_user_by_text(bot, text, *, guild=None):
    """Attempts to find the user by the given text.

    Text can either be a user mention, a server-specific nickname or a
    nickname.

    Args:
        bot (discord.Client): The bot client.
        text (str): The text to search for.
        guild (discord.Guild): The server for which nicknames should be
            considered.

    Returns:
        discord.User: The user with the given name.
    """
    text = text.strip()
    mention = is_user_mention(text)
    if mention:
        user = bot.get_user(mention)
        # Make sure we return the guild-specific member for functions such as
        # display name
        if user is None:
            return None
        member = get(guild.members, id=user.id)
        if member is None:
            return user
        return member

    return find_user_by_nick(bot, text, server=guild)


async def find_message(guild, message_id):
    """Attempts to find the given message.

    Args:
        guild (discord.Guild): The guild in which to search.
        message_id (int): The message ID to look for.

    Returns:
        discord.Message: The message if found, else ``None``.
    """
    for channel in guild.text_channels:
        try:
            message = await channel.fetch_message(message_id)
        except (discord.NotFound, discord.Forbidden):
            continue
        else:
            return message
    return None


def days_delta_between(end, start):
    """Return the amount of days between two points.

    This will disregard the time portion and only look at the date.

    Returns:
        datetime.timedelta: Time between the two dates.
    """
    return end.date() - start.date()


def _join_line(character):
    return character not in "~`*"


def format_helptext(text):
    """Fixes the formatting for the given helptext.

    Args:
        text (str): The (raw) helptext.

    Returns:
        str: The formatted helptext.
    """
    dedented = textwrap.dedent(text)
    stripped = dedented.strip()
    paragraphs = stripped.split("\n\n")
    result = []
    for paragraph in paragraphs:
        paragraph = re.sub(r"\n(?![-~*`])", " ", paragraph)
        paragraph = paragraph.replace("%n", "\n")
        result.append(paragraph)
    return "\n\n".join(result)


def human_timedelta(delta):
    """Gives a human readable representation of the given timedelta.

    This is similar to str(delta) but fully writes out the units.

    Args:
        delta (datetime.timedelta): The timedelta that should be formatted.

    Returns:
        str: A human readable string.
    """
    seconds = round(delta.total_seconds())
    sing_units = ["second", "minute", "hour"]
    units = ["seconds", "minutes", "hours"]
    parts = []
    for sunit, unit in zip(sing_units, units):
        this = seconds % TIME_UNIT_FACTOR
        seconds //= TIME_UNIT_FACTOR
        if this == 1:
            parts.insert(0, f"{this} {sunit}")
        elif this > 1:
            parts.insert(0, f"{this} {unit}")
    return " ".join(parts)


def count_digits(num, base=10):
    """Get the number of digits that are needed to represent the given number.

    This only works for positive numbers and bases that are at least 2.

    Args:
        num (int): The number to represent.
        base (int): The base, 10 by default.

    Returns:
        int: The number of digits needed to represent the number in the given
        base.
    """
    assert base >= 2
    assert num >= 0
    if num == 0:
        return 1
    count = 0
    while num:
        num //= base
        count += 1
    return count


def transposed(matrix):
    """Transposes a given matrix.

    Args:
        matrix (list): The input matrix.

    Returns:
        list: The transposed matrix.
    """
    return list(map(list, zip(*matrix)))


def ascii_table(rows, header=None, map_fns=None):
    """Makes an ASCII art table for the given data.

    Args:
        rows (list): A list of the rows.
        header (list): An optional header row.
        map_fns (list): An optional list of functions to be applied to each
            value before outputting.

    Returns:
        str: The ASCII art table.
    """
    if map_fns:
        map_fns = [
            fun if fun is not None else lambda value: value
            for fun in map_fns
        ]
        rows = [
            [fun(value) for fun, value in zip(map_fns, row)]
            for row in rows
        ]

    if header:
        rows.insert(0, header)
    # Figure out the width of each column
    columns = transposed(rows)
    widths = [max(grapheme.length(str(entry)) for entry in col) for col in columns]

    # Actually render all rows
    result = []
    for row in rows:
        rendered = [" {:{}} ".format(r, widths[i]) for (i, r) in enumerate(row)]
        result.append("|".join(rendered))

    if header:
        # Add 2 for spacing on each side of delimiter
        line = ["-" * (width + 2) for width in widths]
        result.insert(1, "+".join(line))

    return "\n".join(result)


def randint_without(start, end, without):
    """Draws a random integer from the given range, but does not return items
    in ``without``.

    Args:
        start (int): Start, inclusive.
        end (int): End of the range, inclusive.
        without (set, list): Set of integers that should not be returned.

    Returns:
        int: Randomly drawn integer.
    """
    withouts = list(set(without))
    withouts.sort()
    real_end = end - len(withouts)
    draw = random.randint(start, real_end)
    for i in withouts:
        if i <= draw:
            draw += 1
    return draw


def random_timedelta(lower, upper):
    """Returns a random :class:`datetime.timedelta` in the given range
    (inclusive).

    Args:
        lower (datetime.timedelta): Lower limit.
        upper (datetime.timedelta): Upper limit.

    Returns:
        datetime.timedelta: Randomly drawn timedelta in the given range.
    """
    assert lower <= upper
    lower_seconds = lower.total_seconds()
    upper_seconds = upper.total_seconds()
    random_seconds = random.randint(lower_seconds, upper_seconds)
    return datetime.timedelta(seconds=random_seconds)


def sys_uptime():
    """Returns the uptime of the host system as a datetime.timedelta.

    Only works on Linux.

    Returns:
        datetime.timedelta: Host system uptimem or None if it cannot be
        determined.
    """
    try:
        with open("/proc/uptime") as f:
            data = f.read()
    except FileNotFoundError:
        return None
    seconds = float(data.split()[0])
    return datetime.timedelta(seconds=seconds)


def strip_to_seconds(delta):
    """Strips away everything below second resolution in a timedelta.

    Args:
        delta (datetime.timedelta): The timedelta that should be stripped.

    Returns:
        datetime.timedelta: A timedelta representing the same delta, up to the
        resolution of a second.
    """
    whole_seconds = int(delta.total_seconds())
    return datetime.timedelta(seconds=whole_seconds)


class AugmentedShelf:
    """A shelve that also supports providing a key function and a default
    value."""

    def __init__(self, shelf, keyfn=None, default_factory=None):
        """Initializes a new AugmentedShelve.

        Args:
            shelf (shelve.Shelf): The underlying shelf that is used to save data.
            keyfn: The function that is used for each key access.
            default_factory: A factory used to create missing elements.
        """
        self.shelf = shelf
        self.keyfn = keyfn
        if keyfn is None:
            self.keyfn = lambda x: x
        self.default_factory = default_factory

    def __repr__(self):
        return f"<AugmentedShelf({self.shelf!r}, {self.keyfn!r}, {self.default_factory!r})>"

    def __getitem__(self, key):
        k = self.keyfn(key)
        try:
            return self.shelf[k]
        except KeyError:
            if self.default_factory is not None:
                return self.default_factory()
            raise

    def __contains__(self, key):
        k = self.keyfn(key)
        return k in self.shelf

    def get(self, key, default=None):
        """Searches the given item or returns the default value if not found."""
        k = self.keyfn(key)
        return self.shelf.get(k, default)

    def setdefault(self, key, default=None):
        """Sets the item to the default value, if it does not exist yet, and
        returns it.
        """
        if key in self:
            return self[key]

        if default is None and self.default_factory is not None:
            default = self.default_factory()
        self[key] = default
        return default

    def __setitem__(self, key, value):
        k = self.keyfn(key)
        self.shelf[k] = value

    def __delitem__(self, key):
        k = self.keyfn(key)
        del self.shelf[k]

    def pop(self, key):
        """Returns the given item and deletes it from the shelf.

        Args:
            key: The key to delete.

        Returns:
            The object.
        """
        k = self.keyfn(key)
        return self.shelf.pop(k)

    def sync(self):
        """Synchronizes the underlying shelf."""
        self.shelf.sync()


def sql_row_or_default(db, query, default=None):
    """Returns a single SQL row, or the given default.

    Args:
        db: The sqlalchemy database.
        query: The query to run.
        default: The default to return.

    Returns:
        Either a single SQL row or the default value.
    """
    result = db.execute(query)
    row = result.fetchone()
    result.close()
    if row is None:
        return default
    return row


def member_point_table(entries, header):
    """Returns a ASCII table with (display_name, points) for the given entries.

    Args:
        entries (list): List of rows. Each row should consist of a member and
            their points.
        header (list): The header for the row. Should contain two elements.

    Returns:
        str: The ASCII table.
    """
    entries = list(entries)
    entries.sort(key=lambda e: e[1], reverse=True)
    return ascii_table(
        entries,
        header=header,
        map_fns=[attrgetter("display_name"), None]
    )


async def read_resource(path):
    """Reads the resource with the given path.

    The path must be relative to the katsura source root directory.

    Note that this file reads the complete file content into RAM, so for bigger
    files, a different access method should be used.

    Args:
        path (str): Path to the resource file.

    Returns:
        bytes: The file content. If the file could not be found, None is
        returned.
    """
    path = os.path.join(os.path.dirname(__file__), path)
    try:
        with open(path, "rb") as fobj:
            return fobj.read()
    except FileNotFoundError:
        return None


async def read_helptext(topic):
    """Read the helptext with the given topic.

    If the help text could not be found, it will return None instead.

    Helptexts are expected to lie in the ``helptexts/`` directory in the source
    root, and are expected to be encoded with UTF-8.

    Args:
        topic (str): Helptext topic.

    Returns:
        str: The decoded helptext.
    """
    text = await read_resource(f"helptexts/{topic}.md")
    if text is None:
        return None
    return text.decode("utf-8")
