"""Main module of the Discord bot."""
import datetime
import importlib
import logging
import re
import os

from collections import defaultdict

import discord
import sqlalchemy

from sqlalchemy import MetaData

from .permissions import Permissions
from .commands import get_command, CommandError, Context, ExitCommand

LOGGER = logging.getLogger(__name__)

# Times are in UTC time
WURM_TIMES = [
    (1, 0), (4, 0), (8, 0),
    (12, 30), (17, 0), (20, 0),
]

#: List[str]: Names of the wurms, in the same order as :data:`WURM_TIMES`.
WURM_NAMES = [
    "reset", "morning", "brunch",
    "midday", "dinner", "late",
]


class _Proxy(type):
    """A proxy is a class that not only calls the event callback on the class
    itself, but also on each module."""

    EVENTS = [
        'on_error',
        'on_connect',
        'on_disconnect',
        'on_ready',
        'on_shard_ready',
        'on_resumed',
        'on_error',
        'on_socket_raw_receive',
        'on_socket_raw_send',
        'on_typing',
        'on_message',
        'on_message_delete',
        'on_bulk_message_delete',
        'on_raw_message_delete',
        'on_raw_bulk_message_delete',
        'on_message_edit',
        'on_raw_message_edit',
        'on_reaction_add',
        'on_raw_reaction_add',
        'on_reaction_remove',
        'on_raw_reaction_remove',
        'on_reaction_clear',
        'on_raw_reaction_clear',
        'on_private_channel_delete',
        'on_private_channel_create',
        'on_private_channel_update',
        'on_private_channel_pins_update',
        'on_guild_channel_delete',
        'on_guild_channel_create',
        'on_guild_channel_update',
        'on_guild_channel_pins_update',
        'on_guild_integrations_update',
        'on_webhooks_update',
        'on_member_join',
        'on_member_remove',
        'on_member_update',
        'on_user_update',
        'on_guild_join',
        'on_guild_remove',
        'on_guild_update',
        'on_guild_role_create',
        'on_guild_role_delete',
        'on_guild_role_update',
        'on_guild_emojis_update',
        'on_guild_available',
        'on_guild_unavailable',
        'on_voice_state_update',
        'on_member_ban',
        'on_member_unban',
        'on_group_join',
        'on_group_remove',
        'on_relationship_add',
        'on_relationship_remove',
        'on_relationship_update',
        # This one is undocumented, this one gets called for all fully received
        # raw messages.
        # As parameter, the JSON-decoded message will be passed.
        'on_socket_response',
    ]

    def __new__(cls, clsname, superclasses, attrdict):
        for event in cls.EVENTS:
            old_fun = attrdict.get(event, cls.empty_wrapper())
            wrapper = cls.make_wrapper(event, old_fun)
            attrdict[event] = wrapper
        return super().__new__(cls, clsname, superclasses, attrdict)

    @classmethod
    def empty_wrapper(cls):
        """Returns a wrapper that does nothing."""
        async def wrapper(_self, *_args, **__kwargs):
            return None
        return wrapper

    @classmethod
    def make_wrapper(cls, event, old_fun):
        """Makes a wrapper for the given event.

        Args:
            event (str): Name of the event.
            old_fun: Original function.

        Returns:
            A wrapper for the given event.
        """
        async def wrapper(self, *args, **kwargs):
            try:
                for module in self.modules:
                    handler = getattr(module, event, None)
                    if handler is not None:
                        await handler(*args, **kwargs)
                await old_fun(self, *args, **kwargs)
            except Exception:
                LOGGER.exception("Exception occurred in event handler")
                raise
        wrapper.__name__ = event
        return wrapper


class Katsura(discord.Client, metaclass=_Proxy):
    """Main class of the bot.

    Attributes:
        cmdline_args (argparse.Namespace): Command line arguments as passed to the bot.
        config (Config): Bot configuration.
        permissions (Permissions): Permission handler.
        muted_channels (collections.defaultdict): Dictionary that maps guilds
            to channels that should be muted.
        modules (list): List of loaded module instances.
        commands (dict): Mapping of command name to handler function.
        started_at (datetime.datetime): Starting time of the bot.
        metadata (sqlalchemy.schema.MetaData): Database metadata.
        db (sqlalchemy.engine.Engine): Database engine.
    """

    def __init__(self, cmdline_args, config):
        self.cmdline_args = cmdline_args
        self.config = config
        self.permissions = Permissions(config)
        self.muted_channels = defaultdict(set)
        self.modules = []
        self.commands = {}
        self.started_at = None

        self.metadata = MetaData()
        self.db = sqlalchemy.create_engine(config.get("database.uri"))

        self.load_commands(self.__class__)

        intents = discord.Intents.default()
        intents.members = True
        intents.message_content = True
        super().__init__(intents=intents)

    def load_modules_from_config(self):
        """Load all modules that are defined in the configuraiton file."""
        pkg_root = __name__
        cmd_module_names = self.config.get("bot.commands")
        for cmd in cmd_module_names:
            module = importlib.import_module(cmd, pkg_root)
            self.load_commands(module)

        modules = self.config.get("bot.modules")
        for mod_name in modules:
            parts = mod_name.split(".")
            classname = parts[-1]
            path = ".".join(parts[:-1])
            module = importlib.import_module(path, pkg_root)
            self.load_module(getattr(module, classname))

    def load_module(self, cls):
        """Load the module from the given class.

        The class should be a subclass of Module. This automatically creates an
        instance, so do not pass an instance of the class, but rather the class
        itself.

        Args:
            cls: The class to load.
        """
        instance = cls(self)
        instance.init_db(self.metadata)
        self.modules.append(instance)
        self.load_commands(instance)

    def load_commands(self, obj):
        """Load all commands from the given object.

        This searches through all attributes of the object and filters out the
        commands.

        Args:
            obj: The object to load the commands from. Can either be a Python
            module or an instance of a bot module.
        """
        for name in dir(obj):
            attr = getattr(obj, name)
            cmd = get_command(attr)
            if not cmd or not cmd.is_toplevel:
                continue

            self.commands[cmd.name] = attr

    def get_module(self, cls):
        """Returns the module instance of the given class.

        Args:
           cls: Module class that should be searched.

        Returns:
           Instance of the module.
        """
        for module in self.modules:
            if isinstance(module, cls):
                return module
        return None

    def shelvedir(self):
        """Returns the shelve directory for this bot.

        Returns:
           str: The shelve directory.
        """
        from . import module as m
        return self.config.get("database.shelvedir", m.DEFAULT_SHELVEDIR)

    def clear_shelves(self):
        """Clear all shelve files."""
        shelvedir = self.shelvedir()
        files = os.listdir(shelvedir)
        for fname in files:
            if not fname.endswith((".shelf", ".shelf.dir", ".shelf.dat",
                                   ".shelf.db")):
                continue
            path = os.path.join(shelvedir, fname)
            os.unlink(path)

    def setup(self):
        """Make sure the bot is set-up and ready to run.

        This method recursively sets up all modules as well, and makes sure the
        database tables are created.
        """
        os.makedirs(self.shelvedir(), exist_ok=True)

        self.metadata.create_all(self.db)
        for module in self.modules:
            module.setup()

        if self.server_settings is not None:
            self.server_settings.add_validation_hook(
                "bot.prefix.override", _validate_prefix)

    async def setup_routines(self):
        """Call the asynchronous setup routines defined in the configuration."""
        pkg_root = __name__
        for routine_path in self.config.get("bot.setup_routines", []):
            parts = routine_path.split(".")
            fnname = parts[-1]
            path = ".".join(parts[:-1])
            module = importlib.import_module(path, pkg_root)
            routine = getattr(module, fnname)
            await routine(self)

    def run(self, *args, **kwargs):
        """Connect to Discord and block until the bot is exited."""
        self.started_at = datetime.datetime.now(datetime.timezone.utc)
        return super().run(*args, **kwargs)

    def uptime(self):
        """Return the amount of time since the bot has been started.

        Returns:
            datetime.timedelta: Uptime of the bot.
        """
        return datetime.datetime.now(datetime.timezone.utc) - self.started_at

    @property
    def server_settings(self):
        """Returns the server settings instance."""
        return None

    async def on_message(self, message):
        """Event callback for messages.

        Does command parsing and execution.
        """
        if message.author.id == self.user.id:
            return

        channel_whitelist = self.cmdline_args.channel_whitelist
        if channel_whitelist:
            if isinstance(message.channel, discord.abc.PrivateChannel):
                return
            if message.channel.name not in channel_whitelist:
                return

        if message.guild and message.channel.name in self.muted_channels[message.guild.id]:
            return

        text = message.content
        prefix = None
        if message.guild is not None and self.server_settings is not None:
            prefix = self.server_settings.get(message.guild.id, "bot.prefix.override")
        if prefix is None:
            prefix = self.config.get("bot.prefix", "!")

        match = re.match(prefix, text)
        if not match:
            return

        cmdline = text[match.end():]
        # We append "" in the case that the message does not contain a space at
        # all.
        # This allows split[1] to always be set, either to the remaining text
        # or to an empty string.
        split = cmdline.split(" ", 1) + [""]
        cmd = split[0]
        callback = self.commands.get(cmd)
        if not callback:
            return
        try:
            await self.invoke(callback, message, split[1])
        except CommandError as error:
            error_string = f"**[Error]** {error}"
            await message.channel.send(error_string)
        except ExitCommand as exit:
            await message.channel.send(exit.text)

    async def invoke(self, function, message, remainder):
        """Invoke the given function as a callback.

        This function takes a function decorated with @command and runs it.

        Args:
            function: The callback function, as registered in the command dict.
            message (discord.Message): The source message.
            remainder (str): The remaining text, after the command name has
                been split off.
        """
        assert hasattr(function, '_command'), "attempting to invoke a non-command"
        ctx = Context(self, message, remainder)
        # pylint: disable=protected-access
        return await function._command.invoke(function, ctx)

    async def get_help_text(self, topic):
        """Retrieves the helptext for a given topic.

        Args:
            topic (str): Name of the topic to retrieve.

        Return:
            str: The help text, or ``None``.
        """
        # First check if the given topic is a command.
        cmd = self.commands.get(topic)
        if cmd is not None and getattr(cmd, "__doc__", None):
            return cmd.__doc__

        # Gives modules the ability to return help texts
        for module in self.modules:
            text = await module.request_help(topic)
            if text is not None:
                return text

        return None

    def list_help_topics(self):
        """Returns a list of all available help topics.

        Note that this list is not guaranteed to be exhaustive, as modules can
        generate dynamic help texts using the :meth:`Module.request_help`
        method.

        Returns:
            list: A list of available help topics.
        """
        return [
            name for (name, cmd) in self.commands.items()
            if getattr(cmd, "__doc__", None) and not get_command(cmd).hidden
        ]


def _validate_prefix(_server_id, _name, value):
    try:
        re.compile(value)
    except re.error:
        return ["Invalid regex"]
    return []
