Hello, I am the Caffeinator, your friendly neighborhood coffee bot!
I am responsible for getting everyone their required dose of :coffee:.

:pushpin: **First Steps**

Use `!coffee` to get a nice picture of a coffee.

Use `!coffee USERNAME` to give a coffee to the specified user.
Giving away a coffee costs 10 beans, and you start out with 50 by default.

Use `!brew` to brew a drink in the channel.
Brewing costs 15 beans.
Once something has been brewed (by any user), you can use `!drink` to drink it.
*Tip*: Sharing is caring!
Drinking your own brew is *very* discouraged.

You can use `!beans` to view your current amount of available beans and `!coffees` to view your amount of coffees.

If you feel like you have too many coffees but too few beans, you can turn some of them back into beans by using `!dehydrate AMOUNT`, where *AMOUNT* is the amount of coffees that you want to turn back.

:pushpin: **The Bean Aristocracy**

Use `!beanrank` to get an overview over the most wealthy bean possessors on this server. 

:pushpin: **Sharing Is Caring**

Want to make a lot of people happy? Use `!pot` to set up a big coffee pot in
the channel. It's expensive - but anyone that comes in time will get their
share of coffee!

:pushpin: **Want More Details?**

You can use `!help brew`, `!help coffee`, `!help drink`, `!help dehydrate`,
`!help beanrank` and `!help pot` to view the help for those specific commands.

:pushpin: **Adding Custom Coffee Pictures**

You can feed me with coffee pictures by posting a picture to the channel and including the :coffee: emoji in the message text.
That way I will save the picture for later use and might choose it when someone requests a picture of a coffee!
