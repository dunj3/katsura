"""Very basic (administrative) commands."""
from .. import utils
from . import command, CommandError, require, argparser, arg, get_command


@argparser(arg("topic", nargs="?"))
@command("help")
async def cmd_help(bot, message, args):
    """
    **Usage**
    `!help TOPIC`

    Retrieves help for the given topic.
    """
    if not args.topic:
        default_topic = bot.config.get("bot.default_helptopic", "help")
        text = await bot.get_help_text(default_topic)
        if text is None:
            raise CommandError("Default help not found.")
        text = utils.format_helptext(text)
        topics = bot.list_help_topics()
        text += "\n\n" + "Further help topics are: "
        text += ", ".join(topics)
    else:
        text = await bot.get_help_text(args.topic)
        if text is None:
            raise CommandError(f"topic '{args.topic}' not found")
        text = utils.format_helptext(text)
    await message.channel.send(text)


@command("uptime")
async def cmd_uptime(bot, message, _):
    """
    **Usage**
    `!uptime`

    Shows for how long the bot has been running.
    """
    bot_uptime = utils.strip_to_seconds(bot.uptime())
    sys_uptime = utils.strip_to_seconds(utils.sys_uptime())
    lines = [
        f"The bot has been running for {bot_uptime}"
    ]
    if sys_uptime is not None:
        lines.append(
            f"The system has been running for {sys_uptime}"
        )
    await message.channel.send("```" + "\n".join(lines) + "```")


@command("commandlist")
async def cmd_commandlist(bot, message, _):
    """
    **Usage**
    `!commandlist`

    Shows a list of all available commands.
    """
    commands = ", ".join(
        cmdname
        for cmdname, fn in bot.commands.items()
        if not get_command(fn).hidden
    )
    await message.channel.send(f"Available commands: `{commands}`")


@require(".admin.mute")
@argparser(
    arg("--channel"),
    arg("--server-id", type=int),
    arg("--instance"),
    arg("--remove", "-r", action="store_true", default=False),
    override_help=True,
)
@command("mute")
async def cmd_mute(bot, message, args):
    """Mutes the bot on the given channel."""
    if args.instance and args.instance != bot.cmdline_args.instance_name:
        return

    server = args.server_id
    if server is None:
        server = message.guild.id

    channel = args.channel
    if channel is None:
        channel = message.channel.name

    if args.remove:
        bot.muted_channels[server].remove(channel)
    else:
        bot.muted_channels[server].add(channel)


@require(".admin.leave")
@argparser(arg("guild_id", type=int))
@command("leave")
async def cmd_leave(bot, message, args):
    """
    **Usage**
    `!leave GUILD_ID`

    Makes the bot leave the specified Discord server/guild.
    """
    guild = bot.get_guild(args.guild_id)
    await message.channel.send(f"Leaving {guild.name}")
    await guild.leave()


@require(".admin.exit")
@command("exit")
async def cmd_exit(bot, _message, remainder):
    """
    **Usage**
    `!exit [--clear-shelves]`

    Exists the bot. Optionally clears the shelves before exiting.
    """
    if remainder == "--clear-shelves":
        bot.clear_shelves()
    import sys
    sys.exit(0)
