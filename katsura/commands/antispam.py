"""Spam protection module."""

import asyncio
import re
import logging
import time
import datetime
import collections

import pysafebrowsing

from discord.utils import find, get

from sqlalchemy import Table, Column, Text, select

from . import command, require, argparser, arg
from ..module import Module

LOGGER = logging.getLogger(__name__)

A_DAY = datetime.timedelta(hours=24)
MAX_HISTORY = 10

SUSPICIOUS_PHRASES = [
    'discord nitro for free',
    'free nitro',
    'free discord nitro',
    'i will accept all trades',
]

LINK_RE = re.compile(r"https?://[A-Za-z0-9_.-]+", re.I)
DOMAIN_RE = re.compile(r"(?:[0-9A-Za-z_-]+\.)+[A-Za-z]+")


def words(text):
    """Split up the given text into words.

    This removes all punctuation characters.

    Args:
        text (str): Text to split up.

    Returns:
        list: List of words.
    """
    clean_text = re.sub(r"\s+", " ", text)
    clean_text = clean_text.strip()
    clean_text = re.sub(r"[^A-Za-z\s]", "", clean_text)
    return clean_text.lower().split()


def word_variety_score(text):
    """Score the word variety.

    The word variety is a number between 0 and 1, where higher means more
    unique words have been used.

    Args:
        text (str): Input text to score.

    Returns:
        float: Word variety score.
    """
    wordlist = words(text)
    if not wordlist:
        return 1
    return len(set(wordlist)) / len(wordlist)


def text_similarity(text_a, text_b):
    """Check how similar two texts are.

    Args:
        text_a (str): First text.
        text_b (str): Second text.

    Returns:
        float: Text similarity between 0 and 1.
    """
    words_a = set(words(text_a))
    words_b = set(words(text_b))
    return len(words_a & words_b) / len(words_a | words_b)


def history_score(message, history):
    """Compare the whole history of message to a new message.

    Args:
        message: The discord message.
        history (iterable): List of previous messages.

    Returns:
        float: Similarity points for the whole history.
    """
    return sum(text_similarity(message.content, h.content) for h in history)


class SpamUser:
    """Data class with information about a possible spam user."""

    __slots__ = ('score', 'last_update', 'history', 'last_mute')

    def __init__(self):
        self.score = 0
        self.last_update = 0
        self.history = collections.deque(maxlen=MAX_HISTORY)
        self.last_mute = None

    def cleanup_history(self):
        """Clean up old history entries."""
        now = datetime.datetime.now()
        i = 0
        while i < len(self.history):
            msg = self.history[i]
            if now - msg.created_at > A_DAY:
                del self.history[i]
            else:
                i += 1

    def decay_score(self):
        """Apply decay to the spam score based on time passed."""
        new_score = self.score - (time.time() - self.last_update) * 50
        self.score = max(new_score, 0)
        self.last_update = time.time()

    def should_forgive(self):
        """Checks whether the user should be forgiven or not.

        Returns:
            bool: True if the user should be forgiven.
        """
        now = datetime.datetime.now()
        return self.last_mute is None or now - self.last_mute > A_DAY


class AntiSpam(Module):
    """Anti spam module."""

    def init_db(self, metadata):
        self.scams = Table(
            "scams", metadata,
            Column("url_regex", Text, primary_key=True),
        )
        self.bad_domains = Table(
            "bad_domains", metadata,
            Column("domain", Text, primary_key=True),
        )

    def setup(self):
        # {server: {user: history}}
        self.users = collections.defaultdict(
            lambda: collections.defaultdict(SpamUser)
        )

        self.safe_browsing = None
        sb_token = self.bot.config.get("api.safe_browsing")
        if sb_token:
            self.safe_browsing = pysafebrowsing.SafeBrowsing(sb_token)

    def get_config(self, guild):
        """Get the config for the given guild.

        Args:
            guild (discord.Guild): The guild that is being searched.

        Returns:
            dict: Configuration for the given guild.
        """
        if guild is None:
            return None
        return find(
            lambda s: s["guild"] == guild.id,
            self.bot.config.get("antispam", [])
        )

    def calculate_score(self, message):
        """Calculate the spam score for the given message.

        Args:
            message (discord.Message): Message to calculate the score for.

        Returns:
            float: The spam score. Higher means more likely to be spam.
        """
        # Base score for messages
        score = 100

        # Penalty for each user mentioned
        score += len(message.mentions) * 20

        # Penalty for each role mentioned
        score += len(message.role_mentions) * 30

        # Penalty for mentioning @everyone, even if it didn't ping
        if "@everyone" in message.content:
            score += 60

        # Higher variety means more chances that the text is actually good
        variety = word_variety_score(message.content)
        score += 1 / variety

        # Compare text to previous texts
        history = self.users[message.guild][message.author].history
        score += history_score(message, history) * 25

        return score

    async def on_message(self, message):
        """Event handler for incoming messages."""
        # Ignore own messages
        if message.author.id == self.bot.user.id:
            return

        config = self.get_config(message.guild)
        if config is None:
            return

        exempts = config.get("exempt_roles", [])
        if any(role.name in exempts for role in message.author.roles):
            return

        scams = self.load_scams()
        if any(scam.search(message.content) for scam in scams):
            await self.scrub(message, config)
            return
        if self.contains_scam_domain(message.content):
            await self.scrub(message, config)
            return
        if (any(re.search(p, message.content, re.I) for p in SUSPICIOUS_PHRASES)
                and re.search('https?://', message.content, re.I)):
            await self.scrub(message, config)
            return

        if self.safe_browsing:
            await self.check_safe_browsing(message)

        user = self.users[message.guild][message.author]
        user.cleanup_history()
        user.decay_score()

        score = self.calculate_score(message)
        user.score += score

        user.history.append(message)

        if user.score > 400:
            await self.handle_spam(message, user, config)

    async def handle_spam(self, message, user, config):
        """Called when spam has been detected.

        Applies the fitting punishments.

        Args:
            message (discord.Message): Message that caused the spam filter to
                trigger.
            user (SpamUser): Spam user instance.
            config (dict): Spam configuration for the guild.
        """
        logchan = self.bot.get_channel(config["logchan"])
        await logchan.send(
            f"Spamming detected by {message.author.mention} in {message.channel.mention}")

        role = get(message.guild.roles, name=config["muterole"])
        LOGGER.info(f"Adding {role} to {message.author} on {message.guild} because of spamming")
        await message.author.add_roles(role, reason="Anti spam protection")

        # See if the user should be forgiven automatically
        if user.should_forgive():
            user.last_mute = datetime.datetime.now()
            minutes = 5
            if (message.author.joined_at is not None and
                    datetime.datetime.now() - message.author.joined_at < A_DAY):
                minutes = 15
            LOGGER.info(f"Waiting {minutes} minutes before forgiving the user")
            await asyncio.sleep(minutes * 60)
            LOGGER.info(f"Removing {role} from {message.author} on {message.guild}")
            await message.author.remove_roles(
                role, reason="User has been automatically forgiven")
        else:
            await logchan.send("User will not be automatically unmuted "
                               "due to spamming before")
            user.last_mute = datetime.datetime.now()

    async def check_safe_browsing(self, message):
        """Checks the message for safe browsing violations."""
        domains = LINK_RE.findall(message.content)
        if not domains:
            return

        result = self.safe_browsing.lookup_urls(domains)
        bad = any(r["malicious"] for r in result.values())
        if bad:
            await message.channel.send("""\
**Careful:** The previous message contains links that \
are deemed *malicious*. Never give your account information to third-party \
sites!""")
            LOGGER.info(f"Safe Browsing result: {result}")

    def contains_scam_domain(self, text):
        """Checks whether the given text contains a reference to a known scam
        domain.

        Args:
            text (str): The message text.

        Returns:
            True if a scam domain has been found.
        """
        # Find all possible domains
        for domain in DOMAIN_RE.findall(text):
            domain = domain.lower()
            cursor = self.bot.db.execute(
                select([self.bad_domains])
                .where(self.bad_domains.c.domain == domain)
            )
            if cursor.fetchone() is not None:
                return True
        return False

    async def scrub(self, message, config):
        """Called when the given message should be scrubbed.

        This applies to phishing messages for Discord Nitro links and mutes the
        user.

        Args:
            message (discord.Message): The message to scrub.
            config (dict): Spam configuration for the guild.
        """
        logchan = self.bot.get_channel(config["logchan"])
        role = get(message.guild.roles, name=config["muterole"])
        await message.author.add_roles(role, reason="Anti phishing protection")
        LOGGER.info(
            f"Adding {role} to {message.author} on {message.guild} "
            "because of phishing"
        )

        content = message.content
        await message.delete()
        await logchan.send(
            f"Muted {message.author.display_name}, malicious message:"
            "\n\n"
            f"{content}"
        )

        await message.channel.send(
            "The previous message was deleted due to a high chance of "
            "containing malicious links. **Remember to keep your account data"
            " safe and never trust third party sites with your information!**"
        )

    def insert_scam(self, url):
        """Adds the given url as a scam URL to the database.

        Args:
            url (str): The URL to add (a regular expression).
        """
        stmt = self.scams.insert().values(url_regex=url)
        self.bot.db.execute(stmt)

    def load_scams(self):
        """Retrieve all scams from the database.

        Returns:
            list: Returns a list of compiled regular expressions.
        """
        cursor = self.bot.db.execute(select([self.scams]))
        return [re.compile(row.url_regex, re.I) for row in cursor]

    @require(".antispam.addscam")
    @argparser(
        arg("url", help="URL to add as malicious (regular expression)"),
        dumb_split=True,
    )
    @command("add-scam")
    async def cmd_add_scam(self, _bot, message, args):
        """
        **Usage**
        `!add-scam URL`

        Adds the given URL as a scam URL.
        """
        self.insert_scam(args.url)
        await message.channel.send("The pattern has been successfully added")

    @require(".antispam.scams")
    @command("scams")
    async def cmd_scams(self, _bot, message, _args):
        """
        **Usage**
        `!scams`

        List all known Discord scamming sites.
        """
        scams = self.load_scams()
        reply = """\
**Warning:** Do *not* give your account information to \
any of the sites listed below, they are known scamming sites.

```
{}
```
""".format("\n".join(scam.pattern for scam in scams))
        await message.channel.send(reply)
