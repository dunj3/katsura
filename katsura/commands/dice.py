"""Module for random dice based decisions."""

import re
import time
import random
import datetime
import discord

from . import command, CommandError, argparser, arg
from ..module import Module
from .. import utils


WURMS = ["Amber", "Cobalt", "Crimson"]

PRE_DICE = {
    "tag": [
        ":CommiRed:", ":CommiOrange:", ":CommiYellow:", ":CommiGreen:",
        ":CommiCyan:", ":CommiBlue:", ":CommiPurple:", ":CommiPink:",
        ":CommiWhite:",
    ],
    "dungeon": [
        "Ascalonian Catacombs", "Caudecus's Manor", "Twilight Arbor",
        "Sorrow's Embrace", "Citadel of Flame", "Honor of the Waves",
        "Crucible of Eternity", "The Ruined City of Arah",
    ],
    "wing": ["W1", "W2", "W3", "W4", "W5", "W6"],
    "spirits": ["Frost Spirit", "Sun Spirit", "Stone Spirit", "Storm Spirit"],
    "class": [
        # Heavy
        "Herald", "Revenant",
        "Dragonhunter", "Firebrand",
        "Berserker", "Spellbreaker",
        # Medium
        "Daredevil", "Deadeye",
        "Scrapper", "Holosmith",
        "Druid", "Soulbeast",
        # Light
        "Tempest", "Weaver",
        "Chronomancer", "Mirage",
        "Reaper", "Scourge",
    ],
    "keg spot": ["beach", "jumping puzzle", "mast"],
    "mode": ["normal draft", "normal blind", "ARAM", "RGM"],
    "role": ["healer", "chrono", "DPS", "BS"],
    "legend": [
        "Bloodhound", "Gibraltar", "Lifeline", "Pathfinder", "Octane",
        "Wraith", "Bangalore", "Caustic", "Mirage",
    ],
}
DICE_RANGE_RE = re.compile(r"^(-?\d+)\.\.(-?\d+)$")
DICE_NUMBER_RE = re.compile(r"^(-?\d+)$")

DICE_ANSWER = "The die said {}"

DICE_CMD = re.compile("^Can I have the dic?e", re.I)
DICE_ANSWERS = [
    "{name}, for now you will get {wurm}!",
    "Let's give {wurm} to {name}",
    "The dice said {wurm} for {name}",
    ":game_die: {wurm} for {name} :game_die:",
]

REROLLS = {}


class DiceRoll:
    """Represents a roll of a dice.

    This can also remember previous results to prevent them from being chosen
    again.

    Attributes:
        lst (list): List of possible choices, or None.
        start (int): Start integer, or None.
        end (int): End integer, or None.
        blocked (list): List of previous rolls that are blocked for future
            ones.
    """

    def __init__(self, lst=None, start=None, end=None):
        self.lst = lst
        self.start = start
        self.end = end
        self.blocked = []

    def should_troll(self):
        """Returns whether the dice should return a silly result."""
        # pylint: disable=no-self-use
        return random.random() < 0.05

    def block(self, item):
        """Block the given item from reappearing."""
        self.blocked.append(item)

    def draw(self, block=True):
        """Draw an item randomly.

        If block is True, the drawn item is automatically blocked from future
        rolls.
        """
        random.seed()
        if self.lst is not None:
            return self._draw_lst(block)
        if self.start is not None and self.end is not None:
            return self._draw_range(block)
        return None

    def _draw_lst(self, block):
        choices = self.lst[:]
        if block:
            for blocked in self.blocked:
                choices.remove(blocked)
        if not choices:
            return None
        return random.choice(choices)

    def _draw_range(self, block):
        if self.start > self.end:
            return None
        if block and self.end - self.start + 1 <= len(self.blocked):
            return None
        roll = utils.randint_without(self.start, self.end, self.blocked)
        return roll


class Dice(Module):
    """Dice module.

    Only used for the message handler to listen for "Can I have the dice" triggers.
    """

    async def on_message(self, message):
        """Event handler for incoming messages."""
        if DICE_CMD.match(message.content):
            wurm = random.choice(WURMS)
            answer = (
                random.choice(DICE_ANSWERS)
                .format(wurm=wurm, name=message.author.display_name)
            )
            await message.channel.send(answer)


@command("dice")
async def cmd_dice(bot, message, remainder):
    """
    **Usage (1)**
    `!dice option 1, option 2, [...]`

    Randomly selects one of the given options and returns it.

    Options are equally likely, even if some people disagree with that.

    **Usage (2)**
    `!dice key`

    Randomly selects an option out of a pre-defined list.

    Available lists are:
    %%LISTS%%

    **Usage (3)**
    `!dice number`
    `!dice start..end`

    Randomly selects an integer from the given range. If no start is given, 1 is used.
    """
    roll = DiceRoll()
    if remainder:
        choices = remainder.split(",")
    else:
        choices = WURMS
    choices = [c.strip() for c in choices]

    if len(choices) == 1:
        key = choices[0]
        number_match = DICE_NUMBER_RE.match(key)
        range_match = DICE_RANGE_RE.match(key)
        if number_match:
            roll.start = 1
            roll.end = int(number_match.group(1))
        elif range_match:
            roll.start = int(range_match.group(1))
            roll.end = int(range_match.group(2))
        elif key.lower() in PRE_DICE:
            roll.lst = PRE_DICE[key.lower()]
        else:
            roll.lst = [_maybe_mention(bot, message.guild, key)]
    else:
        roll.lst = [_maybe_mention(bot, message.guild, c) for c in choices]

    REROLLS[message.channel] = roll

    if roll.should_troll():
        msg = "I don't know."
    else:
        answer = roll.draw()
        if answer is None:
            msg = "There are no options"
        else:
            roll.block(answer)
            answer = utils.replace_server_emojis(str(answer), message.guild)
            msg = DICE_ANSWER.format(answer)
    await message.channel.send(msg)

cmd_dice.__doc__ = cmd_dice.__doc__.replace(
    "%%LISTS%%", ", ".join(PRE_DICE.keys()))


def _maybe_mention(bot, server, text):
    if server is None:
        return text
    user = utils.find_user_by_nick(bot, text, server=server)
    if user is None:
        return text
    return user.mention


@command("reroll")
async def cmd_reroll(_bot, message, _):
    """
    **Usage**
    `!reroll`

    Re-rolls the dice with the last parameters.

    **Examples**

    ```
    !dice 1..10
    !reroll
    ```
    """
    roll = REROLLS.get(message.channel)
    if roll is None:
        raise CommandError("You need to roll the dice first!")
    answer = roll.draw(False)
    if answer is None:
        msg = "There are no options"
    else:
        roll.block(answer)
        answer = utils.replace_server_emojis(str(answer), message.guild)
        msg = DICE_ANSWER.format(answer)
    await message.channel.send(msg)


@command("reconsider")
async def cmd_reconsider(_bot, message, _):
    """
    **Usage**
    Re-rolls the dice with the last parameters, ignoring previous rolls.

    **Examples**

    ```
    !dice tag
    !reconsider
    !reconsider
    ```
    """
    roll = REROLLS.get(message.channel)
    if roll is None:
        raise CommandError("You need to roll the dice first!")
    answer = roll.draw(True)
    if answer is None:
        msg = "No more options left"
    else:
        roll.block(answer)
        answer = utils.replace_server_emojis(str(answer), message.guild)
        msg = DICE_ANSWER.format(answer)
    await message.channel.send(msg)


@command("re")
async def cmd_re(bot, message, _):
    """
    Alias for `reconsider`.
    """
    return await cmd_reconsider(bot, message, _)


def get_bedtime(now):
    """Returns a random bedtime, starting from the given time.

    Args:
        now (datetime.datetime): The current time.

    Returns:
        datetime.datetime: A random bedtime.
    """
    if now.hour >= 22 or now.hour < 6:
        minutes = random.gauss(30, 15)
        return now + datetime.timedelta(minutes=minutes)

    target_time = datetime.datetime.today().replace(hour=22)
    wiggle = random.gauss(0, 15 * 60)
    delta = datetime.timedelta(seconds=wiggle)
    bedtime = target_time + delta
    return bedtime


@command("whendowegotobed")
async def cmd_whendowegotobed(_bot, message, _):
    """
    **Usage**
    `!whendowegotobed`

    Returns a random bedtime.
    """
    now = datetime.datetime.now()
    if 6 <= now.hour < 8:
        answer = ("Just stay up a bit longer, "
                  "it's not worth getting into bed anymore.")
    else:
        bedtime = get_bedtime(now)
        delta_in_min = (bedtime - now).total_seconds() / 60.0
        if delta_in_min < 0:
            answer = f"You should be asleep since {-delta_in_min:.2f} minutes already"
        elif delta_in_min == 0:
            answer = "Now. :bed: :alarm_clock:"
        elif delta_in_min < 120:
            answer = f"You should go to bed in {delta_in_min:.2f} minutes"
        else:
            answer = f"You should go to bed at {bedtime:%H:%M:%S}"
    await message.channel.send(answer)


@argparser(
    arg("message_id", type=int),
    arg("reaction"),
)
@command("raffle")
async def cmd_raffle(_bot, message, args):
    """
    **Usage**
    `!raffle MESSAGE_ID REACTION`

    Takes all the people that have reacted to the given message with the given
    reaction and chooses one of them at random.
    """
    found_message = await utils.find_message(message.guild, args.message_id)
    if found_message is None:
        raise CommandError("I could not find a message like that")

    reaction = None
    for reaction in found_message.reactions:
        # Unify handling of Unicode and server-specific emoji
        if str(reaction) == str(args.reaction):
            break
    else:
        raise CommandError("I could not find a reaction like that on the given message")

    eligible = [
        member async for member in reaction.users()
        if isinstance(member, discord.Member)
    ]
    await message.channel.send("I am rolling my magic wheel of fortune...")
    time.sleep(3)
    winner = random.choice(eligible)
    await message.channel.send(f"And the winner is {winner.mention}!")
