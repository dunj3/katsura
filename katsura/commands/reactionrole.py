"""ReactionRoles is a module which supports adding and removing roles to users
based on reactions to a specific message.
"""

import logging
from typing import NamedTuple

import discord
from discord.utils import get, find

from ..module import Module
from .. import utils
from . import command, require, CommandError


LOGGER = logging.getLogger(__name__)


class RrStatus(NamedTuple):
    """Reaction role consistency state."""
    okay: set  #: set: Set of users that are consistent.
    only_reaction: set  #: set: Users that reacted, but don't have the role.
    only_role: set  #: set: Users that have the role, but don't have the reaction.
    missing: set  #: set: Users that left the guild in the meantime.


class ReactionRole(Module):
    """ReactionRole module."""

    def __init__(self, bot):
        super().__init__(bot)
        self.rroles = {}
        for item in bot.config.get("reactionroles"):
            self.rroles\
                .setdefault(item['guild'], {})\
                .setdefault(item['message'], {})[item['emoji']] = item['role']
        self._msg_cache = {}

    def get_role_for(self, guild_id, message_id, emoji):
        """Get the role for the specified combintion of guild/message/emoji.

        Args:
            guild_id (int): ID of the guild.
            message_id (int): ID of the message.
            emoji (str): Name of the emoji.

        Returns:
            str: Name of the role to add/remove.
        """
        return self.rroles.get(guild_id, {}).get(message_id, {}).get(emoji, None)

    async def on_raw_reaction_add(self, payload):
        """Event handler for reaction adds."""
        await self._handle_payload(payload, True)

    async def on_raw_reaction_remove(self, payload):
        """Event handler for reaction removals."""
        await self._handle_payload(payload, False)

    async def _handle_payload(self, payload, add):
        if payload.guild_id is None:
            return
        role_name = self.get_role_for(payload.guild_id, payload.message_id, payload.emoji.name)
        if role_name is None:
            return
        guild = self.bot.get_guild(payload.guild_id)
        role = get(guild.roles, name=role_name)
        member = guild.get_member(payload.user_id)
        if member is None:
            # User might have left the guild already
            return
        if add:
            await member.add_roles(role)
        else:
            await member.remove_roles(role)

    async def find_message(self, guild_id, message_id):
        """Finds the message with the given ID in the given guild.

        Args:
            guild_id (int): Numeric ID of the guild.
            message_id (int): Numeric ID of the message.

        Returns:
            discord.Message: The message or None.
        """
        if message_id in self._msg_cache:
            return self._msg_cache[message_id]

        guild = self.bot.get_guild(guild_id)
        if guild is None:
            return

        message = await utils.find_message(guild, message_id)
        if message is not None:
            self._msg_cache[message_id] = message
        return message

    async def check_status(self, guild_id, message_id, emoji_name, role_name):
        """Checks the consistency between the reactions and users with roles.

        Args:
            guild_id (int): Numeric guild ID.
            message_id (int): Numeric message ID.
            emoji_name (str): Name of the emoji, without colons.
            role_name (str): Name of the role.

        Returns:
            RrStatus: A tuple with users that are consistent, that only have
            the reaction, that only have the role and that are missing from the
            Discord completely.
        """
        message = await self.find_message(guild_id, message_id)
        if message is None:
            return None
        reaction = find(
            lambda react:
            getattr(react.emoji, "name", react.emoji) == emoji_name,
            message.reactions
        )
        all_users = await reaction.users().flatten()
        okay = set()
        missing = set()
        only_reaction = set()
        for user in all_users:
            if not isinstance(user, discord.Member):
                missing.add(user)
                continue
            if get(user.roles, name=role_name) is None:
                only_reaction.add(user)
            else:
                okay.add(user)

        guild = self.bot.get_guild(guild_id)
        only_role = set()
        for user in guild.members:
            if get(user.roles, name=role_name) is not None and user not in all_users:
                only_role.add(user)

        return RrStatus(
            okay=okay,
            only_role=only_role,
            only_reaction=only_reaction,
            missing=missing,
        )

    @require(".rroles.status")
    @command("rr-status")
    async def cmd_rr_status(self, _bot, message, _remainder):
        """
        **Usage**
        `!rr-status`

        Checks consistency between reaction on messages and actual user roles.
        """
        guild_id = message.guild.id
        guild_config = self.rroles.get(guild_id, {})
        if not guild_config:
            raise CommandError("No reaction roles set up for this server")

        for message_id, roles in guild_config.items():
            for emoji_name, role_name in roles.items():
                msg = await self.find_message(guild_id, message_id)
                status = await self.check_status(guild_id, message_id, emoji_name, role_name)

                answer = [
                    'Content:',
                    msg.content,
                    '',
                    f'Reaction: `{emoji_name}`',
                    f'Role: `{role_name}`',
                    '',
                    f'Synced: {len(status.okay)} users',
                ]
                _add_block(answer, status.missing, 'Missing:')
                _add_block(answer, status.only_reaction, 'Only reaction:')
                _add_block(answer, status.only_role, 'Only role:')
                await message.channel.send('\n'.join(answer))

    @require(".rroles.fix")
    @command("rr-fix")
    async def cmd_rr_fix(self, _bot, message, _remainder):
        """
        **Usage**
        `!rr-fix`

        Make sure the reaction emojis and user roles are in sync.

        You can check with `!rr-status` what has to be done.
        """
        guild_id = message.guild.id
        guild_config = self.rroles.get(guild_id, {})
        if not guild_config:
            raise CommandError("No reaction roles set up for this server")

        for message_id, roles in guild_config.items():
            for emoji_name, role_name in roles.items():
                msg = await self.find_message(guild_id, message_id)
                status = await self.check_status(guild_id, message_id, emoji_name, role_name)
                role = get(msg.guild.roles, name=role_name)
                reaction = find(
                    lambda react, emoji_name=emoji_name: react.emoji.name == emoji_name,
                    msg.reactions)

                log = []
                for user in status.missing:
                    await reaction.remove(user)
                    log.append(f"- Removed reaction of {user.display_name}")
                for user in status.only_reaction:
                    await user.add_roles(role)
                    log.append(f"- Added role {role.name} to {user.display_name}")

                answer = (
                    "Message:\n"
                    f"{msg.content}\n\n"
                    f"Reaction: `{emoji_name}`\n"
                    f"Role: `{role_name}`\n\n"
                    "Actions:\n```\n"
                )
                answer += "\n".join(log)
                answer += "\n```"
                await message.channel.send(answer)


def _add_block(answer, userset, title):
    if not userset:
        return
    answer.extend([
        title,
        '```',
        _list_users(userset),
        '```',
    ])


def _list_users(userset):
    users = [user.display_name for user in userset]
    users.sort()
    return ', '.join(users)
