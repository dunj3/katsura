"""Generic command system for Katsura.

A command is a simple asynchronous function that is being called with a simple
identifier. For example, if there is a command ``"uptime"``, it can be called
by using ``!uptime``.

.. note:: The prefix (``"!"`` can be configured).

The advantage of using commands over ``on_message`` event listeners is that
commands provide some infrastructure around them. More specifically, they

* Do the required command lookup.
* Have a configurable prefix.
* Can do argument parsing.
* Can do permission checking.

A command function takes three parameters:

* The bot, as an instance of :class:`katsura.Katsura`.
* The message that triggered the command, as :class:`discord.Message`.
* The remaining text, after the command name has been split off, as :class:`str`.

If the command uses argument parsing, it will instead receive a
:class:`argparse.Namespace` as the last argument.

Implementation
--------------

The :func:`command` decorator creates a fitting :class:`Command` instance and
assigns it to ``function._command`` for future use. Note that the function
object itself is returned, to make it easier to work with in regards to runtime
inspection.

This ``_command`` attribute can then be retrieved by using :func:`get_command`.
That way, all the required metadata can be received again.

API Reference
-------------
"""
# We access a lot of _command attributes, as that is where we store the
# metadata. This is fine for this module, so disbale the lint:
# pylint: disable=protected-access

import shlex
from argparse import ArgumentParser, ArgumentTypeError

from .. import utils

#: Special argument type to represent a guild member.
#:
#: Can be passed as ``type=guild_member`` to :func:`arg`.
GUILD_MEMBER = object()


def _monospace(text):
    return f"```{text}```"


class Context:
    # pylint: disable=too-few-public-methods
    """Context of a command invocation. """
    __slots__ = ('bot', 'message', 'cmdline')

    def __init__(self, bot, message, cmdline):
        self.bot = bot
        self.message = message
        self.cmdline = cmdline


class CmdArgParser(ArgumentParser):
    """A special ArgumentParser for bot commands."""

    def __init__(self, bot, cmd, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bot = bot
        self.command = cmd

    def error(self, message):
        raise CommandError(message)

    def print_help(self, file=None):
        docstring = self.command.function.__doc__
        if docstring and not self.command.override_help:
            raise ExitCommand(utils.format_helptext(docstring))
        raise ExitCommand(_monospace(self.format_help()))

    def print_usage(self, file=None):
        raise ExitCommand(_monospace(self.format_usage()))


class CommandError(Exception):
    """An exception for errors that occur during command execution."""

    def __init__(self, msg):
        super().__init__()
        self.msg = msg

    def __str__(self):
        return self.msg


class ExitCommand(Exception):
    """An exception to exit a command early with the given text."""

    def __init__(self, text):
        super().__init__()
        self.text = text

    def __str__(self):
        return self.text


class Command:
    """Represents a command available to bot users.

    Attributes:
        name (str): Name of the command.
        function: Underlying callable (async) function object.
        arguments (list): A list of arguments added to the function, if any.
        use_argparse (bool): Whether arguments should be parsed or left
            untouched.
        permissions (list): A list of required permissions.
        override_help (bool): True if the automatically generated help should
            be used instead of the function docstring.
        dumb_split (bool): See :func:`argparser` for more information about
            dumb splitting.
        subcommands (dict): A dictionary of subcommands.
        is_toplevel (bool): Indicating whether this is a top level command.
        hidden (bool): True if the command should be hidden from the
            commandlist.
    """

    def __init__(self, name, function):
        self.name = name
        self.function = function
        self.arguments = []
        self.use_argparse = False
        self.permissions = []
        self.override_help = False
        self.dumb_split = False
        self.subcommands = {}
        self.is_toplevel = False
        self.hidden = False

    def check_permissions(self, context):
        """Check if the given invocation has the required permissions.

        Args:
            context (~katsura.commands.Context): Invocation context.

        Returns:
            list: A list of missing permissions. If the list is empty, the
            command may be exeucted by the user.
        """
        return [
            permission
            for permission in self.permissions
            if not context.bot.permissions.check(
                context.message.author,
                context.message.guild,
                permission
            )
        ]

    async def invoke(self, function, context):
        """Invoke the command for the given context.

        Args:
            function: The callable function.

                This needs to be passed extra and may differ from
                :attr:`~Command.function`, as it might be a bound method of an
                instance.
            context (~katsura.commands.Context): Invocation context.
        """
        perms = self.check_permissions(context)
        if perms:
            missing_perms = ", ".join(perms)
            raise CommandError(f"Missing permissions: `{missing_perms}`")

        if self.subcommands:
            if await self.delegate_subcommand(function, context):
                return None

        funcarg = context.cmdline
        if self.use_argparse:
            parser = self.make_argparser(context)
            if not context.cmdline.strip():
                parts = []
            elif self.dumb_split:
                parts = context.cmdline.split(" ", len(self.arguments) - 1)
            else:
                parts = shlex.split(context.cmdline)
            funcarg = parser.parse_args(parts)

        return await function(context.bot, context.message, funcarg)

    def make_argparser(self, context):
        """Create the argument parser for this command.

        Args:
            context (~katsura.commands.Context): Invocation context.

        Returns:
            CmdArgParser: A fitting argument parser for this command.
        """
        parser = CmdArgParser(context.bot, self, prog=self.name)
        for args, kwargs in self.arguments:
            kwargs = kwargs.copy()

            if kwargs.get("type", None) is GUILD_MEMBER:
                kwargs["type"] = _arg_guild_member(context.bot, context.message.guild)

            parser.add_argument(*args, **kwargs)
        return parser

    def register_subcommand(self, cmd):
        """Register a subcommand to this command.

        Args:
            cmd (Command): The subcommand to register.
        """
        self.subcommands[cmd.name] = cmd

    async def delegate_subcommand(self, function, context):
        """Attempt to delegate this to a subcommand.

        Args:
            function: The callable function.
            context (~katsura.commands.Context): Invocation context.

        Returns:
            bool: True if the command was successfully delegated, false otherwise.
        """
        if not context.cmdline:
            return False

        subcmd, remainder, *_ = context.cmdline.split(" ", 1) + [""]
        if subcmd not in self.subcommands:
            return False

        cmd = self.subcommands[subcmd]
        new_context = Context(context.bot, context.message, remainder)
        # Carry through the self object if needed
        if getattr(function, "__self__", None):
            new_fun = cmd.function.__get__(function.__self__)
        else:
            new_fun = cmd.funcion
        await cmd.invoke(new_fun, new_context)
        return True


def _arg_guild_member(bot, guild):
    def validator(text):
        member = utils.find_user_by_text(bot, text, guild=guild)
        if member is None:
            raise ArgumentTypeError(f"A user named '{text}' could not be found.")
        return member
    return validator


def require(permission):
    """Mark a command to require a specific permission.

    This function is a decorator.

    Args:
        permission (str): The permission that this command requires.
    """
    def decorator(function):
        assert hasattr(function, '_command'), "using @require on a non-command"
        function._command.permissions.append(permission)
        return function
    return decorator


def command(name, hidden=False):
    """Mark a function as being a command.

    This function is a decorator.

    Args:
        name (str): Name of the command.
        hidden (bool): Whether the command should be hidden.
    """
    def decorator(function):
        cmd = Command(name, function)
        cmd.is_toplevel = True
        cmd.hidden = hidden
        function._command = cmd
        return function
    return decorator


def argparser(*arglist, override_help=False, dumb_split=False):
    """Add argument parsing to a command.

    This function takes in :func:`arg` calls to define arguments.

    This function is a decorator.

    Args:
        override_help (bool): True if the automatically generated help should
            be used.
        dumb_split (bool): If True, the splitting algorithm is changed from
            :func:`shlex.split` to a simple split on spaces, with a max
            splitting count matching the argument count.

            This is useful for commands which take a username as the last
            parameter, as that will allow those commands to work without
            needing to quote the user name.
    """
    def decorator(function):
        assert hasattr(function, '_command'), "using @argparser on a non-command"
        function._command.use_argparse = True
        function._command.override_help = override_help
        function._command.dumb_split = dumb_split
        function._command.arguments.extend(arglist)
        return function
    return decorator


def arg(*args, **kwargs):
    """Defines a single argument.

    The arguments and keyword arguments are the same as for
    :meth:`argparse.ArgumentParser.add_argument`.

    This should be used as an argument to :func:`argparser`.
    """
    return (args, kwargs)


def get_command_name(fun):
    """Return the command name of the function, if available.

    Args:
        fun: The function to examine.

    Returns:
        str: The name of the command, or None.
    """
    cmd = getattr(fun, "_command", None)
    if cmd is None:
        return None
    return cmd.name


def get_command(fun):
    """Return the command object of the function, if available.

    Args:
        fun: The function to examine.

    Returns:
        Command: The associated command instance.
    """
    return getattr(fun, "_command", None)


def subcommand(parent, name):
    """Registers a subcommand to an already existing command.

    This function is a decorator.

    Args:
        parent: The parent command.
        name (str): The subcommand name.
    """
    assert hasattr(parent, '_command'), "registering a subcommand to a non-command"

    def decorator(function):
        cmd = Command(name, function)
        function._command = cmd
        parent._command.register_subcommand(cmd)
        return function
    return decorator
