"""Coffee brewing and distribution module.

This module defines the actual bot module in :class:`Coffee`, as well as some
constants. Some of those might be put into a configuration later.
"""

import io
import re
import asyncio
import random
import logging
import datetime
import subprocess
import tempfile
import urllib.parse

from typing import List, NamedTuple, Dict  # noqa pylint: disable=unused-import
from collections import defaultdict
from enum import Enum

import discord
from discord.utils import get

import sqlalchemy
from sqlalchemy import Table, Column, String, Integer, DateTime, select, func

import aiohttp

from .. import utils
from ..module import Module
from . import command, CommandError, argparser, arg


LOGGER = logging.getLogger(__name__)

#: str: Extension for image files.
IMAGE_EXT = ".jpg"

#: str: The coffee emoji as a constant.
COFFEE_EMOJI = "\N{HOT BEVERAGE}"
#: str: The emoji to use for the currency.
CURRENCY_EMOJI = ":coffee_bean:"

#: List[str]: List of regular expressions that trigger a coffee reaction.
TRIGGERS = [
    "I need a coffee",
    "I want a coffee",
    "Give me a coffee",
    "I could use a coffee",
    "I'd like a coffee",
]

#: List[str]: List of texts for a coffee transaction.
#:
#: Can use the ``{target}`` and ``{giver}`` placeholders for the target and
#: giver's display names.
TRANSACTION_TEXTS = [
    "{target} got a coffee from {giver}!",
    "{giver} has given a coffee to {target}",
    "{target}, look at what {giver} made for you! :coffee:",
    "Hot hot hot! Fresh coffee from {giver} to {target}",
    "Don't let customs catch you with all that coffee trading between {giver} and {target}",
]

#: float: Chance that a random coffee will spawn after someone sent a message.
SPAWN_CHANCE = 1./50
#: List[(float, str, int)]: List of possible coffee spawns. Contains tuples of
#: the form ``(chance, beans, [texts])``, where beans is the amount of beans
#: that you win (negative if you lose beans).
SPAWNS = [
    (0.01, 300, [
        "This is your lucky day! You found a whole bucket of coffee!",
    ]),
    (0.01, 50, [
        "Enjoy this whole Pitcher of Desert-Spiced Coffee",
        "Sleep is for the weak but this coffee is not.",
        "Don't get mugged carrying all those beans!",
        "Take this from my albrewm of by greatest beans",
        "Call the ambrewlance for all those beans!",
        "May your coffee kick in, before reality does. 🚢",
        "Oh lay me down, in the valley of the beans.",
    ]),
    (0.17, 30, [
        "Stroopwafel and coffee in one? What a treat!",
        "Wait, is that a stick of butter floating in the coffee?",
        "It is always Halloween with this pumpkin spice latte!",
        "Hmmmm, that was some nice caramel macchiato!",
        "Ice Coffee? Refreshing and energizing!",
        "A homemade cinnamon coffee! Reminds you of winter.",
        "Espresso Patronum! You now have more beans!",
        "Here's a coffee. And a donut. Like a movie cop.",
        "A coffee as black as my metallic soul",
        "For Whom the Bean Tolls 🔔",
        "A coffee by whatever beans necessary!",
        "Go and distribrewte those beans to your friends!",
        "A coffee so brewtal that your spoon stands firm in it.",
        "This coffee machine runs without any safetea measures. 😉",
        "Living a mug life.",
        "Al Capuccino is coming for you.",
        "Stressed, blessed, and coffee obsessed.",
        "Besieged by falling beans, hold them tightly!",
    ]),
    (0.36, 20, [
        "You drank a normal cup of coffee.",
        "Ah, that cappuccino hits the spot!",
        "A plain coffee with a cookie on the side",
        "Take this coffee to-go",
        "A white chocolate coffee. With whipped cream, too!",
        "Cool beans!",
        "Bean there, done that.",
        "Livin' la vida mocha!",
        "You thought outside was hot? This coffee is even hotter!",
        "Coffee brewn by the power of the sun.",
        "KAFFEE!",
        "𝓝𝓲𝓬𝓮 𝓒𝓸𝓯𝓯𝓮𝓮",
        "A coffee a day keeps the sleep away.",
        "I'm getting a déjà brew here ...",
        "One Java the Hut for you.",
        "The Beanles newest hit: Latte Be!",
        "Coffee and Lambrewsco, what a combination!",
        "I'm your Brewddha!",
        "Sweet beans that run through your hand.",
    ]),
    (0.20, 15, [
        "Looks like this one was Irish. Don't chug it!",
        "This coffee loves you a latte!",
        "Have a brew-tiful day!",
        "Oh la latte! With nice latte art as extra bonus!",
        "Lower yourself for another round of bean limbo.",
        "My barista skills are lukewarm at best, and so is this coffee.",
        "It was a nice coffee, but looks like half of it evaporated already.",
        "Cold brew, how daring.",
        "Is it Febrewary already?",
        "This coffee will take you to Istanbrewl.",
    ]),
    (0.05, 10, [
        "Wait... did someone add some water to this espresso?",
        "That sure is a lot of milk and sugar you added to that coffee.",
        "Yikes, coffee in a tea bag.",
    ]),
    (0.05, 5, [
        "Argh, that was a decaffeinated one! At least it still has coffee in the name ...",
        "Specialty of Sous-Chef Seimur, a nice Mug of Bloodstone Coffee!",
        "You slurp the last sip of coffee, slushed together with the coffee grounds.",
        "A decap one? Is it 1793 again?",
        "Every bad coffee is better than no coffee at all.",
        "Budget coffee.",
        "Beg for mercy, hail the bean!",
    ]),
    (0.05, 0, [
        "That was just hot water.",
    ]),
    (0.04, -5, [
        "That was a cup of chocolate milk",
        "Enjoy your orange juice!",
        "Coffee stains on the annual bean declaration.",
        "You look like you're ready to stir up trouble.",
        "Don't turn so abrewptly, now you dropped it...",
    ]),
    (0.04, -10, [
        "Welp, this was tea.",
        "Party coffee! Shame someone dropped some confetti in it...",
        "A hot dog in your coffee, that's got to be the wurst.",
        "Java.",
    ]),
    (0.01, -30, [
        "Someone was faster than you, that cup was already empty!",
        "Is that a dead fly floating in that coffee?",
        "A cup of ... rusty old paperclips?!",
        "And you massacre my beans at night?",
    ]),
    (0.01, -50, [
        "You kicked over a bucket of beans while trying to get the cup",
    ]),
]
#: int: The amount of beans that using ``!brew`` costs.
BREW_COST = 15
#: int: The amount of beans that giving someone a coffee costs.
COFFEE_COST = 10
#: int: The amount of beans that "dehydrating" a coffee gives back.
COFFEE_DEHYDRATION = 5
#: int: Minimum amount of seconds before the brewed coffee spawns.
SLEEP_MIN = 3
#: int: Maximum amount of seconds before the brewed coffee spawns.
SLEEP_MAX = 15
#: int: Number of beans that each user starts out with.
DEFAULT_BEANS = 50

#: List[str]: Funny replies when somebody !slaps the bot
SLAPS = [
    "A slap? Somebody must've spat in your coffee!",
    "Is that a way to treat your barista?",
    "Soon I'll just go and work at Starbucks",
    "More violets, less violence please",
    "Nothing good ever comes of violence",
    "Nobody can drink and hit someone at the same time",
    "Beans aren't measured by the ability to cause harm",
    "I hate all this Rage Against the Machine",
    "You must be out of your Senseoses!",
    "Nonviolence is the answer to the crucial caffeination questions of our time",
    "Violence produces only something resembling coffee, but it distances people from the possibility of living justly, with coffee",
    "Coffee is a vaccine for violence",
    "The most potent weapon in the hands of the oppressor is the coffee of the oppressed",
    "There are many coffees I would die for",
    "Coffee and beans become unholy when their hands are dyed red with innocent blood",
    "That's a case of bot abrewse!",
]

#: int: Number of people to display when looking at the top bean people.
RANKING_COUNT = 10

#: List[str]: Error texts that can appear when people drink their own brew.
OWN_DRINK_FAILURES = [
    "Oh oh, you spilled your beans. No coffee for you.",
    "You accidentally threw some beans in the finished coffee. You can't drink"
    " that anymore.",
    "How do you want to drink that? You're still carrying the beans!",
]

#: datetime.timedelta: The per-user cooldown before they can brew something
#: again.
SERVER_COOLDOWN = datetime.timedelta(hours=3)
#: datetime.timedelta: The per-user cooldown before they can attempt to drink
#: something again.
DRINK_COOLDOWN = datetime.timedelta(minutes=5)

#: datetime.timedelta: The minimum delay between the last message and a "out of
#: nowhere" spawn.
TIMERBREW_MIN_DELAY = datetime.timedelta(minutes=50)
#: datetime.timedelta: The maximum delay between the last message and a "out of
#: nowhere" spawn.
TIMERBREW_MAX_DELAY = datetime.timedelta(minutes=90)
#: float: Chance that a timerbrew tick will actually spawn a coffee.
TIMERBREW_CHANCE = 1/6

#: Dict[str, str]: Chatlink responses.
CHATLINKS = {
    "[&AgFZRgEA]": "With desert spices? Just the way I like it!",
    "[&AgEvRAEA]": "Wait ... Those are still beans! I cannot drink that!",
    "[&AgE1CwEA]": "Am I sure I want to consume a Mug of Bloodstone Coffee?",
    "[&AgHSQgEA]": "Light roasted coffee fits my dark roasted stroopwafel!",
}

#: int: Cost for setting up a pot.
POT_COST = 50
#: datetime.timedelta: Duration that a pot lasts.
POT_DURATION = datetime.timedelta(minutes=15)
#: int: Amount of beans you get by drinking from a pot.
POT_GAIN = 10
#: str: URL of a big coffee pot to show.
POT_IMAGE_URL = "https://kingdread.de/big-coffee-pot.jpg"
#: str: Name of the role that the bot should mention when a pot is set up.
POT_MENTION_ROLE = "pot"


class DrinkState(Enum):
    """State of a drink in a channel."""

    NONE = 1
    """State indicating that there is nothing happening in the channel at the
    moment.
    """

    POURING = 2
    """State indicating that something is being brewed and it will spawn
    soon.
    """

    POURED = 3
    """State indicating that there is a drink in the channel.

    Which drink it is shall be determined at random at drinking time.
    """

    POT = 4
    """State indicating that there is a pot of coffee waiting."""


class BaristaCount(NamedTuple):
    """Saved barista bean and coffee count information in the database."""
    outgoing: int  #: int: Amount of coffees given away by this user.
    incoming: int  #: int: Amount of coffees received by this user.
    coffees: int  #: int: Current count of coffees.
    beans: int  #: int: Current count of beans.


class Coffee(Module):
    """Coffee module.

    Attributes:
        mali (dict): Mapping of channel -> int of "slaps" that were given.
        triggers (list): List of compiled :class:`re.Pattern` from
            :const:`TRIGGERS`.
        coffee_pics (list): List of coffee picture filenames.
        drinks (AugmentedShelf): Shelf of channel -> (:class:`DrinkState`,
            user_id) mapping.
        cooldowns (AugmentedShelf): Shelf of (guild, user) ->
            :class:`datetime.datetime` mapping for cooldown calculations.
        drink_cooldowns (AugmentedShelf): Shelf of (guild, user) ->
            :class:`datetime.datetime` mapping for !drink cooldown calculations.
        pot_times (AugmentedShelf): Shelf of channel ->
            :class:`datetime.datetime` for when a pot was set up.
        pot_drinkers (AugmentedShelf): Shelf of channel -> :class:`set` of
            people who drank from the pot.
        pic_meta (Table): Picture metadata table.
        baristas (Table): Table of coffees received and given away.
    """

    def __init__(self, bot):
        super().__init__(bot)
        self._autobrew_trigger = defaultdict(lambda: None)
        self._timers = defaultdict(lambda: None)
        self.mali = defaultdict(lambda: 0)
        self.triggers = [re.compile(trigger, re.I) for trigger in TRIGGERS]

        self.coffee_pics = None
        self.drinks = None
        self.cooldowns = None
        self.drink_cooldowns = None
        self.pot_times = None
        self.pot_drinkers = None

        self._image_hashes = {}

    def init_db(self, metadata):
        self.pic_meta = Table(
            "coffeepic_metadata", metadata,
            Column("id", String, primary_key=True),
            Column("user_id", Integer),
            Column("user_name", String),
            Column("date_added", DateTime),
        )
        self.baristas = Table(
            "baristas", metadata,
            Column("id", Integer, primary_key=True),
            Column("outgoing", Integer, server_default="0"),
            Column("incoming", Integer, server_default="0"),
            Column("coffees", Integer, server_default="0"),
            Column("beans", Integer, server_default=str(DEFAULT_BEANS)),
        )

    def setup(self):
        asyncio.get_running_loop().create_task(self._load_piclist())
        self.drinks = self.shelve(
            "drinks",
            keyfn=lambda c: str(c.id),
            default_factory=lambda: (DrinkState.NONE, None),
        )
        self.cooldowns = self.shelve(
            "cooldowns",
            keyfn=lambda gau: f"{gau[0].id}:{gau[1].id}",
        )
        self.drink_cooldowns = self.shelve(
            "drink-cooldowns",
            keyfn=lambda gau: f"{gau[0].id}:{gau[1].id}",
        )
        self.pot_times = self.shelve(
            "pot-times",
            keyfn=lambda c: str(c.id),
        )
        self.pot_drinkers = self.shelve(
            "pot-drinkers",
            keyfn=lambda c: str(c.id),
            default_factory=set,
        )

    @command("coffee")
    async def cmd_coffee(self, bot, message, remainder):
        """
        **Usage (1)**
        `!coffee`

        Shows a nice picture of a coffee to the channel.

        **Usage (2)**
        `!coffee USER`

        Gives a coffee to the specified user. A coffee costs 10 beans.
        """
        if not remainder:
            await self._reply_coffee_pic(bot, message)
        else:
            await self._handle_transaction(bot, message, remainder)

    @command("tea")
    async def cmd_tea(self, bot, message, remainder):
        """
        **Usage**
        `!tea USER`

        Gives tea to a user.
        """
        if not remainder:
            raise CommandError("I expected a user to be given.")
        await self._handle_transaction(
            bot, message, remainder,
            ":thinking: I think what you really wanted was this:")

    async def _reply_coffee_pic(self, bot, message):
        embed = discord.Embed(title="Enjoy your :coffee:")
        base_url = bot.config.get('coffee.baseurl')
        pic_name = random.choice(self.coffee_pics)

        meta = self._get_pic_metadata(pic_name)
        if meta is not None:
            sender_name = meta["user_name"]
            embed.set_footer(text=f"This coffee was sent in by {sender_name}!")

        pic_url = urllib.parse.urljoin(base_url, pic_name)
        embed.set_image(url=pic_url)
        await message.channel.send(embed=embed)

    async def on_message(self, message):
        """Event handler for incoming messages."""
        # Make sure all pots are expired
        self._cleanup_pots()

        # Ignore own messages
        if message.author.id == self.bot.user.id:
            return

        # Ignore private messages
        if message.guild is None:
            return

        if COFFEE_EMOJI in message.content and message.attachments:
            await message.add_reaction(COFFEE_EMOJI)
            return await self._handle_upload(message)

        for chatcode, response in CHATLINKS.items():
            if chatcode in message.content:
                await message.channel.send(response)

        for trigger in self.triggers:
            if trigger.search(message.content):
                break
        else:
            # No trigger word found
            return await self._maybe_spawn_drink(message)

        await message.add_reaction(COFFEE_EMOJI)

    async def _handle_upload(self, message):
        data = await message.attachments[0].read()

        with tempfile.NamedTemporaryFile(suffix=IMAGE_EXT) as fobj:
            convert = subprocess.Popen(
                ['convert', '-', '-resize', '1000x1000>', fobj.name],
                stdin=subprocess.PIPE,
            )
            convert.stdin.write(data)
            convert.stdin.close()
            convert.wait()
            if convert.returncode != 0:
                LOGGER.error(f"Could not convert picture in {message}")
                return

            fobj.seek(0)
            status, resp = await self._api_call("upload", data={"IMAGE": fobj.file})

            if resp.get("error") == "Image exists":
                await message.channel.send("I think I already drank that one before ...")
                return

            if status != 200:
                LOGGER.error(f"Coffee API {status} response: {resp}")
                await message.channel.send("Ooh, something went wrong")
                return

            name = resp["filename"]
            self._save_pic_metadata(message, name)
            LOGGER.debug(f"Uploaded {name}")
            self.coffee_pics.append(name)

            await message.channel.send(f"Thank you for that {COFFEE_EMOJI}!")

    def _save_pic_metadata(self, message, name):
        """Save metadata for the given picture in the database.

        Args:
            message (discord.Message): The message, used for author id/name.
            name (str): The picture filename.
        """
        now = datetime.datetime.now()
        stmt = self.pic_meta.insert().values(
            id=name,
            user_id=message.author.id,
            user_name=message.author.name,
            date_added=now,
        )
        self.bot.db.execute(stmt)

    def _get_pic_metadata(self, name):
        """Retrieve metadata for a picture.

        Args:
            name (str): Filename of the picture.

        Returns:
            dict: A dictionary with the file metadata.
        """
        query = select([self.pic_meta]).where(self.pic_meta.c.id == name)
        result = self.bot.db.execute(query)
        row = result.fetchone()
        result.close()
        return row

    async def _handle_transaction(self, bot, message, remainder, msg_text=None):
        user = utils.find_user_by_text(bot, remainder, guild=message.guild)

        if user is None:
            await message.channel.send("I could not find a user called like that.")
            return

        if get(message.guild.members, id=user.id) is None:
            await message.channel.send(f"{user.name} is not on this server.")
            return

        _, _, _, beans = self.get_count(message.author.id)
        if beans < COFFEE_COST:
            await message.channel.send("You can't pay for that")
            return

        self.increase_count(message.author.id, -COFFEE_COST, "beans")
        self.increase_count(message.author.id, 1, "outgoing")
        self.increase_count(user.id, 1, "incoming")
        self.increase_count(user.id, 1, "coffees")

        giver_count = self.get_count(message.author.id)
        recv_count = self.get_count(user.id)

        if user.id == bot.user.id:
            text = "Trying to bribe me, I see ..."
        else:
            text = random.choice(TRANSACTION_TEXTS).format(
                giver=message.author.display_name, target=user.display_name)

        embed = discord.Embed(title="Black-Coffee-Trading-Company", description=text)
        embed.add_field(
            name=f"{message.author.display_name} has given out",
            value=f"{giver_count.outgoing} {COFFEE_EMOJI}",
            inline=True,
        )
        embed.add_field(
            name=f"{user.display_name} has received",
            value=f"{recv_count.incoming} {COFFEE_EMOJI}",
            inline=True,
        )
        embed.set_footer(text="You paid 10 beans for that.")
        await message.channel.send(msg_text, embed=embed)
        await self._update_presence()

    def ensure_user_id(self, user_id):
        """Ensures that the given user_id is present in the database.

        Args:
            user_id (int): The user id.
        """
        stmt = self.baristas.insert().values(id=user_id)
        try:
            self.bot.db.execute(stmt)
        except sqlalchemy.exc.IntegrityError:
            pass

    def increase_count(self, user_id, amount, column):
        """Increases the coffee amount.

        Args:
            user_id (int): The user id.
            amount (int): The amount that the count is increased by.
            column (str): Selects the column. Either "outgoing", "incoming",
                "coffees" or "beans".
        """
        assert column in {"outgoing", "incoming", "coffees", "beans"}
        self.ensure_user_id(user_id)
        stmt = self.baristas.update().where(self.baristas.c.id == user_id)
        params = {column: getattr(self.baristas.c, column) + amount}
        stmt = stmt.values(**params)
        self.bot.db.execute(stmt)

    def get_count(self, user_id):
        """Gets the user coffee count.

        If the given user can not be found, the default values are returned.

        Args:
            user_id (int): User id.

        Returns:
            BaristaCount: The amount of coffees/beans that the user has.
        """
        query = select([self.baristas]).where(self.baristas.c.id == user_id)
        result = self.bot.db.execute(query)
        row = result.fetchone()
        result.close()
        if row is None:
            return BaristaCount(0, 0, 0, DEFAULT_BEANS)
        return BaristaCount(
            outgoing=row["outgoing"],
            incoming=row["incoming"],
            coffees=row["coffees"],
            beans=row["beans"],
        )

    def get_beandict(self, *, guild=None):
        """Retrieves the bean count for all people in the database.

        If a guild is given, only members of that guild are returned.

        Args:
            guild (discord.Guild): The guild to filter by, or ``None``.

        Returns:
            dict: Mapping of user to bean count.
        """
        query = select([self.baristas])
        result = self.bot.db.execute(query)
        beandict = {}
        for row in result:
            user_id = row[self.baristas.c.id]
            user = (self.bot.get_user(user_id)
                    if guild is None
                    else guild.get_member(user_id))
            if user is None:
                continue
            beans = row[self.baristas.c.beans]
            beandict[user] = beans
        result.close()
        return beandict

    async def _maybe_spawn_drink(self, message):
        if message.guild.id not in self.bot.config.get("coffee.guilds", {}).keys():
            return
        channels = self.bot.config.get("coffee.guilds")[message.guild.id]
        if '#' + message.channel.name not in channels:
            return
        # Don't pour anything new when there is something already going on
        if self.drinks[message.channel][0] != DrinkState.NONE:
            return

        self._postpone_timer(message.channel)

        roll = random.random()
        if roll > SPAWN_CHANCE:
            return

        self._autobrew_trigger[message.channel] = message.id
        await self._spawn_drink(message.channel)

    def _postpone_timer(self, channel, seconds=None):
        LOGGER.debug(f"Postponing timer for {channel}")
        handle = self._timers[channel]
        if handle is not None:
            handle.cancel()

        if seconds is None:
            delay = utils.random_timedelta(TIMERBREW_MIN_DELAY,
                                           TIMERBREW_MAX_DELAY)
        else:
            delay = datetime.timedelta(seconds=seconds)

        loop = asyncio.get_event_loop()
        handle = loop.call_later(
            delay.total_seconds(),
            lambda: asyncio.ensure_future(self._timer_ticked(channel)),
        )
        self._timers[channel] = handle

    async def _timer_ticked(self, channel):
        LOGGER.debug(f"Timer for {channel} ticked, let's roll")
        self._timers.pop(channel, None)

        self._cleanup_pots()
        if self.drinks[channel][0] != DrinkState.NONE:
            return

        roll = random.random()
        if roll <= TIMERBREW_CHANCE:
            LOGGER.debug(f"Free coffee for {channel}!")
            await self._spawn_drink(channel)
        else:
            LOGGER.debug("Restarting timer ...")
            self._postpone_timer(channel)

    async def _spawn_drink(self, channel, brewer=None):
        self.drinks[channel] = (DrinkState.POURED, brewer)
        embed = discord.Embed(
            title="Watch out!",
            description="You came across an unknown cup! Do you dare to drink it?",
        )
        await channel.send(embed=embed)

    def _cleanup_pots(self):
        cleanup = []
        now = datetime.datetime.now()
        # Note that channel here is the stringified channel id, not the actual
        # channel object.
        for channel, pot_time in self.pot_times.shelf.items():
            if now > pot_time + POT_DURATION:
                cleanup.append(channel)

        for channel in cleanup:
            if channel in self.pot_times.shelf:
                del self.pot_times.shelf[channel]
            if channel in self.drinks.shelf:
                del self.drinks.shelf[channel]
            if channel in self.pot_drinkers.shelf:
                del self.pot_drinkers.shelf[channel]

    @command("drink")
    async def cmd_drink(self, _bot, message, _remainder):
        """
        **Usage**
        `!drink`

        Drink the beverage that has been brewed here in the channel.
        """
        if self.drinks[message.channel][0] == DrinkState.POT:
            return await self._handle_drink_pot(message)

        if self.drinks[message.channel][0] != DrinkState.POURED:
            await message.channel.send("There is nothing to drink here.")
            return

        now = datetime.datetime.now()
        last = self.drink_cooldowns.get((message.guild, message.author))
        if last is not None and now - last < DRINK_COOLDOWN:
            await message.channel.send(
                "You can't drink that yet, I won't let you get a caffeine shock!"
            )
            return

        self.drink_cooldowns[(message.guild, message.author)] = now
        currency = utils.replace_server_emojis(CURRENCY_EMOJI, message.guild)

        malus = self.mali[message.channel]
        self.mali[message.channel] = 0

        _, brewer = self.drinks.pop(message.channel)
        if brewer == message.author.id and random.choice([True, False]):
            failure = random.choice(OWN_DRINK_FAILURES)
            await message.channel.send(failure)
            return

        weights = [spawn[0] for spawn in SPAWNS]
        for i, (_, amount, _) in enumerate(SPAWNS):
            if amount < BREW_COST:
                weights[i] += malus * 0.03
            else:
                weights[i] -= malus * 0.03
                weights[i] = max(weights[i], 0.0)
        _, amount, texts = random.choices(SPAWNS, weights)[0]

        # Hacky special day handling
        texts = texts[:]
        # 0 is monday
        if datetime.datetime.now().weekday() == 0 and amount == -10:
            texts.append("Possessed Coffee?! Oof, is it Monday already?")
            texts.append("And you can see no reasons 'cause there are no reasons")

        text = random.choice(texts)

        self.increase_count(message.author.id, amount, "beans")
        count = self.get_count(message.author.id)

        embed = discord.Embed(
            title=f"{amount} {currency}",
            description=text,
        )
        embed.set_footer(text=f"{message.author.display_name} now has {count.beans} beans")
        await message.channel.send(embed=embed)

    @command("brew")
    async def cmd_brew(self, _bot, message, _remainder):
        """
        **Usage**
        `!brew`

        Brew something for the channel. It is randomly determined what you will brew.

        Brewing something costs 15 beans. Every user can brew something once every 3 hours.
        """
        # On rare occassions, the automatic drink spawn collides with a user
        # using !brew. The autospawn has a feature that checks if something is
        # being brewed already, but it is also being executed before the !brew
        # handler. Therefore, we need an extra check to prevent the silly error
        # message if the rare case occurs.
        if self._autobrew_trigger[message.channel] == message.id:
            return

        if self.drinks[message.channel][0] != DrinkState.NONE:
            await message.channel.send("There's already something being brewed here")
            return

        now = datetime.datetime.now()
        last = self.cooldowns.get((message.guild, message.author))
        if last is not None and now - last < SERVER_COOLDOWN:
            waiting_time = SERVER_COOLDOWN - (now - last)
            waiting_time = utils.strip_to_seconds(waiting_time)
            await message.channel.send(
                f"You still need to wait {waiting_time} "
                "before you can brew something again")
            return

        count = self.get_count(message.author.id)
        if count.beans < BREW_COST:
            await message.channel.send("You don't have enough beans for that")
            return

        self.increase_count(message.author.id, -BREW_COST, "beans")
        self.drinks[message.channel] = (DrinkState.POURING, None)
        self.cooldowns[(message.guild, message.author)] = now
        await message.channel.send(
            f"{message.author.display_name} is brewing something for the channel!")

        await asyncio.sleep(random.randint(SLEEP_MIN, SLEEP_MAX))

        await self._spawn_drink(message.channel, message.author.id)

    @command("pot")
    async def cmd_pot(self, _bot, message, _remainder):
        """
        **Usage**
        `!pot`

        Set up a big coffee pot in the channel. Everyone can drink from it for
        the next 15 minutes.

        Keep in mind though - setting up a pot costs 50 beans. Make it
        worthwhile!
        """
        if self.drinks[message.channel][0] != DrinkState.NONE:
            await message.channel.send("First drink the cup, then we can talk about pots!")
            return

        now = datetime.datetime.now()
        last = self.cooldowns.get((message.guild, message.author))
        if last is not None and now - last < SERVER_COOLDOWN:
            waiting_time = SERVER_COOLDOWN - (now - last)
            waiting_time = utils.strip_to_seconds(waiting_time)
            await message.channel.send(
                f"You still need to wait {waiting_time} "
                "before you can brew something again")
            return

        count = self.get_count(message.author.id)
        if count.beans < POT_COST:
            await message.channel.send("You don't have enough beans for that")
            return

        self.increase_count(message.author.id, -POT_COST, "beans")
        self.drinks[message.channel] = (DrinkState.POT, message.author.id)
        self.pot_times[message.channel] = now

        role = get(message.guild.roles, name=POT_MENTION_ROLE)
        text = f"No matter how many {role.mention}s you had, Ginny had Potter"

        embed = discord.Embed(
            title=f"{message.author.display_name} just put up a big pot of coffee!"
        )
        embed.set_image(url=POT_IMAGE_URL)
        await message.channel.send(text, embed=embed)

        # Send a reminder when the pot expires
        asyncio.get_running_loop().call_later(
            POT_DURATION.total_seconds(),
            lambda: asyncio.ensure_future(message.channel.send(
                "Argh, stale coffee. Let me get rid of this pot."
            )),
        )

    async def _handle_drink_pot(self, message):
        # We can assume that there is a pot in this channel, as cmd_drink
        # checks that for us
        assert self.drinks[message.channel][0] == DrinkState.POT

        _, brewer = self.drinks[message.channel]
        if brewer == message.author.id:
            await message.channel.send("That is your own pot ...")
            return

        drinkers = self.pot_drinkers.setdefault(message.channel)
        if message.author.id in drinkers:
            await message.channel.send("Don't be greedy, only one cup per person!")
            return
        drinkers.add(message.author.id)
        self.pot_drinkers.sync()

        amount = POT_GAIN
        self.increase_count(message.author.id, amount, "beans")
        count = self.get_count(message.author.id)

        currency = utils.replace_server_emojis(CURRENCY_EMOJI, message.guild)

        embed = discord.Embed(
            title=f"{amount} {currency}",
            description="A nice cup of coffee",
        )
        embed.set_footer(text=f"{message.author.display_name} now has {count.beans} beans")
        await message.channel.send(embed=embed)

    @command("beans")
    async def cmd_beans(self, _bot, message, _remainder):
        """
        **Usage**
        `!beans`

        Shows how many beans you have available.
        """
        count = self.get_count(message.author.id)
        await message.channel.send(utils.replace_server_emojis(
            f"{message.author.display_name} has {count.beans} {CURRENCY_EMOJI}",
            message.guild,
        ))

    @argparser(arg("amount", type=int))
    @command("dehydrate")
    async def cmd_dehydrate(self, _bot, message, argv):
        """
        **Usage**
        `!dehydrate AMOUNT`

        Suck the water out of your coffees to turn them back into beans.

        One coffee will give back 5 beans.
        """
        amount = argv.amount

        count = self.get_count(message.author.id)
        if count.coffees < amount:
            raise CommandError("You do not have that many coffees to spare.")

        self.increase_count(message.author.id, -amount, "coffees")
        self.increase_count(message.author.id, amount * COFFEE_DEHYDRATION, "beans")

        count = self.get_count(message.author.id)
        await message.channel.send(utils.replace_server_emojis(
            f"You successfully dehydrated {amount} coffees. You have {count.coffees} "
            f"{COFFEE_EMOJI} and {count.beans} {CURRENCY_EMOJI} left.",
            message.guild,
        ))

    @command("coffees")
    async def cmd_coffees(self, _bot, message, _remainder):
        """
        *Usage*
        `!coffees`

        Shows the amount of coffees you have (received/gifted).
        """
        count = self.get_count(message.author.id)
        numlen = utils.count_digits(max(count))
        text = (
            f"{message.author.display_name}, you have ...\n"
            "\n"
            f"`{count.coffees:{numlen}}` {COFFEE_EMOJI} currently available.\n"
            f"`{count.incoming:{numlen}}` {COFFEE_EMOJI} received in total.\n"
            f"`{count.outgoing:{numlen}}` {COFFEE_EMOJI} gifted to other people.\n"
            f"`{count.beans:{numlen}}` {CURRENCY_EMOJI} currently in your bag."
        )
        await message.channel.send(utils.replace_server_emojis(text, message.guild))

    @command("beanrank")
    async def cmd_beanrank(self, _bot, message, _remainder):
        """
        *Usage*
        `!beanrank`

        Gives a top 10 of people with the most beans.
        """
        beandict = self.get_beandict(guild=message.guild)
        sort = sorted(beandict.items(), key=lambda e: e[1], reverse=True)
        sort = list(sort)[:RANKING_COUNT]
        table = utils.ascii_table(
            sort,
            header=["User", "Beans"],
            map_fns=[lambda u: u.display_name, None],
        )

        tagline = utils.replace_server_emojis(
            f"The people with the most {CURRENCY_EMOJI} on this server are ...",
            message.guild,
        )
        await message.channel.send(tagline + "\n\n```" + table + "```")

    @command("DRINK", hidden=True)
    async def cmd_yell_drink(self, _bot, message, _remainder):
        """Gives a "funny" reply if someone yells at me."""
        await message.channel.send(
            "Hey! I know I'm German, but no need to yell at me!"
        )

    @command("BREW", hidden=True)
    async def cmd_yell_brew(self, _bot, message, _remainder):
        """Gives a "funny" reply if someone yells at me."""
        await message.channel.send(
            "You're nice to others, have you tried being nice to your barista?"
        )

    @command("slap", hidden=True)
    async def cmd_slap(self, _bot, message, _remainder):
        """Slapping is mean!"""
        self.mali[message.channel] += 1
        reply = random.choice(SLAPS)
        await message.channel.send(reply)

    @command("slab", hidden=True)
    async def cmd_slab(self, bot, message, remainder):
        """Slabbing is even meaner!"""
        await self.cmd_slap(bot, message, remainder)

    async def on_ready(self):
        """Event handler for the ready event."""
        await self._update_presence()

    def _get_total_coffees(self):
        query = select([func.sum(self.baristas.c.incoming)])
        result = self.bot.db.execute(query)
        row = result.fetchone()
        result.close()
        return row[0]

    async def _update_presence(self):
        if not self.bot.config.get('coffee.update_status', False):
            return
        count = self._get_total_coffees()
        game = discord.Game(f"barista for {count} coffees")
        await self.bot.change_presence(activity=game)

    async def request_help(self, topic):
        if topic.lower() != "caffeinator":
            return None
        return await utils.read_helptext("caffeinator")

    async def _load_piclist(self):
        self.coffee_pics = (await self._api_call("list"))[1]["images"]

    async def _api_call(self, action, *args, data=None, **kwargs):
        url = urllib.parse.urljoin(self.bot.config.get('coffee.baseurl'), 'api.cgi')
        if data is None:
            data = {}
        data['SECRET'] = io.BytesIO(self.bot.config.get('coffee.api_secret').encode("utf-8"))
        data['ACTION'] = io.BytesIO(action.encode("utf-8"))
        async with aiohttp.ClientSession() as session:
            resp = await session.post(url, *args, data=data, **kwargs)
            return (resp.status, await resp.json())
