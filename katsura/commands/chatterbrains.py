"""ReactionRoles is a module which supports adding and removing roles to users
based on reactions to a specific message.
"""

import logging

from discord.utils import get

from ..module import Module


LOGGER = logging.getLogger(__name__)


class Chatterbrains(Module):
    """Various stuff for the Chatterbrains Discord."""

    async def on_member_join(self, member):
        # Chatterbrains ID
        if member.guild.id != 358022435305684995:
            return
        LOGGER.info(f"{member} joined Chatterbrains!")
        # Assign the Chatterling role on join
        role = get(member.guild.roles, name="Chatterling")
        await member.add_roles(role)
